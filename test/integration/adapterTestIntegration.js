/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-paragon_pathfinder',
      type: 'ParagonPathfinder',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const ParagonPathfinder = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Paragon_pathfinder Adapter Test', () => {
  describe('ParagonPathfinder Class Tests', () => {
    const a = new ParagonPathfinder(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */
    describe('#getTopologyV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyId(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyV2TopologyId(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyV2TopologyId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdStream - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdStream(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdStream', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdStatus(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdStatusAnalyticsCollection - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdStatusAnalyticsCollection(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdStatusAnalyticsCollection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdStatusAnalyticsDatabase - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdStatusAnalyticsDatabase(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdStatusAnalyticsDatabase', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdStatusPce - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdStatusPce(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdStatusPce', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdStatusTopologyAcquisition - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdStatusTopologyAcquisition(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdStatusTopologyAcquisition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdStatusPathComputationServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdStatusPathComputationServer(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdStatusPathComputationServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdStatusTransportTopologyAcquisition - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdStatusTransportTopologyAcquisition(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdStatusTransportTopologyAcquisition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdNodes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdNodes(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdNodes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPostTopologyV2TopologyIdNodesBodyParam = {
      name: 'string',
      topoObjectType: 'node',
      topologyIndex: null
    };
    describe('#postTopologyV2TopologyIdNodes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTopologyV2TopologyIdNodes(555, topologyPostTopologyV2TopologyIdNodesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'postTopologyV2TopologyIdNodes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdNodesSearch - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesSearch(555, {}, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdNodesSearch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdNodesStream - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesStream(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdNodesStream', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPostTopologyV2TopologyIdNodesBulkBodyParam = [
      {
        AutonomousSystem: {
          asNumber: null
        },
        activeAssurance: {
          agentPath: 'string'
        },
        comment: 'string',
        design: {
          delay: 2,
          simulation: {
            canFail: true
          },
          userCost: 6
        },
        epeProperties: {
          bandwidth: 3,
          externalRates: [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
          ],
          internalRate: 9,
          peeringRatePlan: [
            {
              bound: 9,
              rate: 6
            },
            {
              bound: 3,
              rate: 9
            },
            {
              bound: 3,
              rate: 7
            },
            {
              bound: 8,
              rate: 4
            },
            {
              bound: 2,
              rate: 10
            },
            {
              bound: 6,
              rate: 2
            },
            {
              bound: 4,
              rate: 2
            },
            {
              bound: 2,
              rate: 6
            },
            {
              bound: 2,
              rate: 1
            }
          ]
        },
        extraIpAddresses: [
          null,
          null,
          null,
          null,
          null,
          null
        ],
        hostName: 'string',
        ipRole: 'Access',
        layer: 'string',
        name: 'string',
        nodeType: 'string',
        prefixes: [
          {
            SR: {
              algo: 6,
              flags: 7,
              index: 7
            },
            SRv6: {}
          },
          {
            SR: {
              algo: 4,
              flags: 1,
              index: 2
            },
            SRv6: {}
          },
          {
            SR: {
              algo: 1,
              flags: 8,
              index: 5
            },
            SRv6: {}
          },
          {
            SR: {
              algo: 3,
              flags: 2,
              index: 4
            },
            SRv6: {}
          },
          {
            SR: {
              algo: 7,
              flags: 5,
              index: 7
            },
            SRv6: {}
          },
          {
            SR: {
              algo: 4,
              flags: 3,
              index: 8
            },
            SRv6: {}
          },
          {
            SR: {
              algo: 10,
              flags: 10,
              index: 8
            },
            SRv6: {}
          },
          {
            SR: {
              algo: 10,
              flags: 3,
              index: 10
            },
            SRv6: {}
          }
        ],
        protocols: {
          ISIS: {
            TERouterId: 'string',
            TERouterIdIPv6: 'string',
            area: 'string',
            isoAddress: 'string',
            routerId: 'string',
            routerIdIPv6: 'string'
          },
          OSPF: {
            TERouterId: 'string',
            TERouterIdIPv6: 'string',
            routerId: 'string',
            routerIdIPv6: 'string'
          },
          PCEP: {
            extensions: {
              'lsp-association-protection': true
            }
          },
          SR: {
            SRGBs: [
              {
                range: 4,
                start: 2
              },
              {
                range: 3,
                start: 4
              },
              {
                range: 7,
                start: 3
              },
              {
                range: 9,
                start: 2
              },
              {
                range: 3,
                start: 4
              },
              {
                range: 3,
                start: 1
              },
              {
                range: 3,
                start: 1
              }
            ],
            capabilities: {
              unrestrictedFirstHop: false
            },
            enabled: false,
            nodeCapabilities: 9
          },
          SRv6: {
            enabled: true,
            msdType: {
              baseMplsImposition: 5,
              erld: 3,
              srhMaxEndD: 9,
              srhMaxEndPop: 8,
              srhMaxHEncaps: 5,
              srhMaxLs: 6
            },
            nodeCapabilities: 8
          },
          management: {
            address: null,
            operatingSystem: 'string',
            operatingSystemVersion: 'string',
            vendor: 'string'
          }
        },
        pseudoNode: false,
        site: 'string',
        slices: [
          {
            sliceId: 2
          },
          {
            sliceId: 4
          },
          {
            sliceId: 8
          },
          {
            sliceId: 10
          },
          {
            sliceId: 10
          }
        ],
        switchingCapabilities: [
          {
            encoding: 'lsp-encoding-sdh',
            switchingCapability: 'switching-dcsc'
          },
          {
            encoding: 'lsp-encoding-pdh',
            switchingCapability: 'switching-evpl'
          },
          {
            encoding: 'lsp-encoding-digital-wrapper',
            switchingCapability: 'switching-fsc'
          },
          {
            encoding: 'lsp-encoding-lambda',
            switchingCapability: 'switching-evpl'
          },
          {
            encoding: 'lsp-encoding-digital-wrapper',
            switchingCapability: 'switching-tdm'
          },
          {
            encoding: 'lsp-encoding-packet',
            switchingCapability: 'switching-dcsc'
          }
        ],
        topoObjectType: 'node',
        topology: {
          coordinates: {
            bbox: [
              4,
              7,
              7,
              9,
              2
            ],
            crs: {
              properties: {},
              type: 'string'
            }
          }
        },
        topologyIndex: null,
        userProperties: {}
      }
    ];
    describe('#postTopologyV2TopologyIdNodesBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTopologyV2TopologyIdNodesBulk(555, topologyPostTopologyV2TopologyIdNodesBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'postTopologyV2TopologyIdNodesBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPutTopologyV2TopologyIdNodesBulkBodyParam = [
      {
        AutonomousSystem: {
          asNumber: null
        },
        activeAssurance: {
          agentPath: 'string'
        },
        comment: 'string',
        design: {
          delay: 2,
          simulation: {
            canFail: true
          }
        },
        deviceUuid: 'string',
        epeProperties: {
          bandwidth: 4,
          externalRates: [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
          ],
          internalRate: 10,
          peeringRatePlan: [
            {
              bound: 9,
              rate: 8
            }
          ]
        },
        extraIpAddresses: [
          null,
          null,
          null,
          null,
          null,
          null,
          null
        ],
        hostName: 'string',
        id: 'string',
        ipRole: 'Regular',
        layer: 'string',
        name: 'string',
        nodeIndex: 6,
        nodeType: 'string',
        prefixes: [
          {
            SR: {
              algo: 6,
              flags: 9,
              index: 7
            },
            SRv6: {}
          },
          {
            SR: {
              algo: 4,
              flags: 2,
              index: 4
            },
            SRv6: {}
          },
          {
            SR: {
              algo: 7,
              flags: 10,
              index: 6
            },
            SRv6: {}
          },
          {
            SR: {
              algo: 6,
              flags: 10,
              index: 4
            },
            SRv6: {}
          },
          {
            SR: {
              algo: 8,
              flags: 7,
              index: 10
            },
            SRv6: {}
          },
          {
            SR: {
              algo: 8,
              flags: 9,
              index: 5
            },
            SRv6: {}
          }
        ],
        protocols: {
          ISIS: {
            TERouterId: 'string',
            TERouterIdIPv6: 'string',
            area: 'string',
            isoAddress: 'string',
            routerId: 'string',
            routerIdIPv6: 'string'
          },
          OSPF: {
            TERouterId: 'string',
            TERouterIdIPv6: 'string',
            routerId: 'string',
            routerIdIPv6: 'string'
          },
          PCEP: {
            extensions: {
              'lsp-association-protection': true
            }
          },
          SR: {
            SRGBs: [
              {
                range: 2,
                start: 8
              },
              {
                range: 9,
                start: 1
              }
            ],
            capabilities: {
              unrestrictedFirstHop: false
            },
            enabled: true,
            nodeCapabilities: 2
          },
          SRv6: {
            enabled: false,
            msdType: {
              baseMplsImposition: 10,
              erld: 4,
              srhMaxEndD: 1,
              srhMaxEndPop: 7,
              srhMaxHEncaps: 1,
              srhMaxLs: 5
            },
            nodeCapabilities: 3
          },
          management: {
            address: null,
            operatingSystem: 'string',
            operatingSystemVersion: 'string',
            vendor: 'string'
          }
        },
        pseudoNode: false,
        site: 'string',
        slices: [
          {
            sliceId: 5
          },
          {
            sliceId: 6
          },
          {
            sliceId: 7
          },
          {
            sliceId: 3
          },
          {
            sliceId: 9
          },
          {
            sliceId: 2
          },
          {
            sliceId: 1
          }
        ],
        switchingCapabilities: [
          {
            encoding: 'lsp-encoding-digital-wrapper',
            switchingCapability: 'switching-l2sc'
          },
          {
            encoding: 'lsp-encoding-lambda',
            switchingCapability: 'switching-evpl'
          },
          {
            encoding: 'lsp-encoding-line',
            switchingCapability: 'switching-tdm'
          },
          {
            encoding: 'lsp-encoding-oduk',
            switchingCapability: 'switching-dcsc'
          }
        ],
        topoObjectType: 'node',
        topology: {
          coordinates: {
            bbox: [
              6,
              10,
              5,
              4,
              8,
              5
            ],
            crs: {
              properties: {},
              type: 'string'
            }
          }
        },
        topologyIndex: null,
        userProperties: {}
      }
    ];
    describe('#putTopologyV2TopologyIdNodesBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTopologyV2TopologyIdNodesBulk(555, topologyPutTopologyV2TopologyIdNodesBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'putTopologyV2TopologyIdNodesBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPatchTopologyV2TopologyIdNodesBulkBodyParam = [
      {
        nodeIndex: 5,
        patch: [
          {
            path: 'string'
          },
          {
            path: 'string'
          },
          {
            path: 'string'
          },
          {
            path: 'string'
          },
          {
            path: 'string'
          },
          {
            path: 'string'
          },
          {
            path: 'string'
          }
        ]
      }
    ];
    describe('#patchTopologyV2TopologyIdNodesBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTopologyV2TopologyIdNodesBulk(555, topologyPatchTopologyV2TopologyIdNodesBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'patchTopologyV2TopologyIdNodesBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdNodesBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyV2TopologyIdNodesBulk(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyV2TopologyIdNodesBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdNodesNodeIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesNodeIndex(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdNodesNodeIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPutTopologyV2TopologyIdNodesNodeIndexBodyParam = {
      nodeIndex: 4,
      topoObjectType: 'node',
      topologyIndex: null
    };
    describe('#putTopologyV2TopologyIdNodesNodeIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTopologyV2TopologyIdNodesNodeIndex(555, 555, topologyPutTopologyV2TopologyIdNodesNodeIndexBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'putTopologyV2TopologyIdNodesNodeIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPatchTopologyV2TopologyIdNodesNodeIndexBodyParam = [
      {
        path: 'string'
      }
    ];
    describe('#patchTopologyV2TopologyIdNodesNodeIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTopologyV2TopologyIdNodesNodeIndex(555, 555, topologyPatchTopologyV2TopologyIdNodesNodeIndexBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'patchTopologyV2TopologyIdNodesNodeIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdNodesNodeIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyV2TopologyIdNodesNodeIndex(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyV2TopologyIdNodesNodeIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdNodesNodeIndexHistory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesNodeIndexHistory(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdNodesNodeIndexHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdNodesNodeIndexUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesNodeIndexUser(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdNodesNodeIndexUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdNodesNodeIndexBgpLs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesNodeIndexBgpLs(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdNodesNodeIndexBgpLs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdNodesNodeIndexDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesNodeIndexDevice(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdNodesNodeIndexDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdLinks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdLinks(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdLinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPostTopologyV2TopologyIdLinksBodyParam = {
      endA: {
        topoObjectType: 'interface'
      },
      endZ: {
        topoObjectType: 'interface'
      },
      topoObjectType: 'link',
      topologyIndex: null
    };
    describe('#postTopologyV2TopologyIdLinks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTopologyV2TopologyIdLinks(555, topologyPostTopologyV2TopologyIdLinksBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'postTopologyV2TopologyIdLinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdLinksStream - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksStream(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdLinksStream', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdLinksUtilization - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksUtilization(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdLinksUtilization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdLinksSearch - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksSearch(555, {}, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdLinksSearch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdLinksLinkIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksLinkIndex(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdLinksLinkIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPutTopologyV2TopologyIdLinksLinkIndexBodyParam = {
      endA: {
        topoObjectType: 'interface'
      },
      endZ: {
        topoObjectType: 'interface'
      },
      linkIndex: 6,
      topoObjectType: 'link',
      topologyIndex: null
    };
    describe('#putTopologyV2TopologyIdLinksLinkIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTopologyV2TopologyIdLinksLinkIndex(555, 555, topologyPutTopologyV2TopologyIdLinksLinkIndexBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'putTopologyV2TopologyIdLinksLinkIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPatchTopologyV2TopologyIdLinksLinkIndexBodyParam = [
      {
        path: 'string'
      }
    ];
    describe('#patchTopologyV2TopologyIdLinksLinkIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTopologyV2TopologyIdLinksLinkIndex(555, 555, topologyPatchTopologyV2TopologyIdLinksLinkIndexBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'patchTopologyV2TopologyIdLinksLinkIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdLinksLinkIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyV2TopologyIdLinksLinkIndex(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyV2TopologyIdLinksLinkIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdLinksLinkIndexHistory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksLinkIndexHistory(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdLinksLinkIndexHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdLinksLinkIndexDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksLinkIndexDevice(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdLinksLinkIndexDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPutTopologyV2TopologyIdLinksLinkIndexDeviceBodyParam = {
      endA: {
        topoObjectType: 'interface'
      },
      endZ: {
        topoObjectType: 'interface'
      },
      linkIndex: 4,
      topoObjectType: 'link',
      topologyIndex: null
    };
    describe('#putTopologyV2TopologyIdLinksLinkIndexDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTopologyV2TopologyIdLinksLinkIndexDevice(555, 555, topologyPutTopologyV2TopologyIdLinksLinkIndexDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'putTopologyV2TopologyIdLinksLinkIndexDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPatchTopologyV2TopologyIdLinksLinkIndexDeviceBodyParam = [
      {
        path: 'string'
      }
    ];
    describe('#patchTopologyV2TopologyIdLinksLinkIndexDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTopologyV2TopologyIdLinksLinkIndexDevice(555, 555, topologyPatchTopologyV2TopologyIdLinksLinkIndexDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'patchTopologyV2TopologyIdLinksLinkIndexDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdLinksLinkIndexUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksLinkIndexUser(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdLinksLinkIndexUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPutTopologyV2TopologyIdLinksLinkIndexUserBodyParam = {
      endA: {
        topoObjectType: 'interface'
      },
      endZ: {
        topoObjectType: 'interface'
      },
      linkIndex: 8,
      topoObjectType: 'link',
      topologyIndex: null
    };
    describe('#putTopologyV2TopologyIdLinksLinkIndexUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTopologyV2TopologyIdLinksLinkIndexUser(555, 555, topologyPutTopologyV2TopologyIdLinksLinkIndexUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'putTopologyV2TopologyIdLinksLinkIndexUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPatchTopologyV2TopologyIdLinksLinkIndexUserBodyParam = [
      {
        path: 'string'
      }
    ];
    describe('#patchTopologyV2TopologyIdLinksLinkIndexUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTopologyV2TopologyIdLinksLinkIndexUser(555, 555, topologyPatchTopologyV2TopologyIdLinksLinkIndexUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'patchTopologyV2TopologyIdLinksLinkIndexUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdLinksLinkIndexBgpLs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksLinkIndexBgpLs(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdLinksLinkIndexBgpLs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdTeLsps - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdTeLsps(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdTeLsps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPostTopologyV2TopologyIdTeLspsBodyParam = {};
    describe('#postTopologyV2TopologyIdTeLsps - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTopologyV2TopologyIdTeLsps(555, topologyPostTopologyV2TopologyIdTeLspsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'postTopologyV2TopologyIdTeLsps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdTeLspsSearch - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdTeLspsSearch(555, {}, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdTeLspsSearch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdTeLspsStream - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdTeLspsStream(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdTeLspsStream', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPostTopologyV2TopologyIdTeLspsBulkBodyParam = [
      null
    ];
    describe('#postTopologyV2TopologyIdTeLspsBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTopologyV2TopologyIdTeLspsBulk(555, topologyPostTopologyV2TopologyIdTeLspsBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'postTopologyV2TopologyIdTeLspsBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPutTopologyV2TopologyIdTeLspsBulkBodyParam = [
      {
        collectedProperties: {
          adminStatus: 'Shutdown',
          bandwidth: null,
          bindingSID: 5,
          calculatedEro: [
            null,
            null
          ],
          color: null,
          comment: 'string',
          controllerStatus: {},
          correlatedRROHopCount: 9,
          design: {
            adminGroups: {
              attributeExclude: 6,
              attributeIncludeAll: 8,
              attributeIncludeAny: 8
            },
            coroutedPairId: 'string',
            diversityGroup: 'string',
            diversityGroupRoutingPriority: 1,
            diversityLevel: 'site',
            maxCost: 1,
            maxDelay: 3,
            maxHop: 9,
            minimumDiversityLevel: 'srlg',
            reroutingBehavior: 'noRerouting',
            routingMethod: 'default',
            slices: {
              sliceExclude: [
                {
                  sliceId: 9
                },
                {
                  sliceId: 8
                },
                {
                  sliceId: 9
                },
                {
                  sliceId: 10
                },
                {
                  sliceId: 10
                }
              ],
              sliceIncludeAll: [
                {
                  sliceId: 6
                },
                {
                  sliceId: 6
                },
                {
                  sliceId: 10
                },
                {
                  sliceId: 10
                },
                {
                  sliceId: 2
                },
                {
                  sliceId: 8
                },
                {
                  sliceId: 2
                },
                {
                  sliceId: 10
                },
                {
                  sliceId: 6
                }
              ],
              sliceIncludeAny: [
                {
                  sliceId: 10
                },
                {
                  sliceId: 7
                },
                {
                  sliceId: 5
                },
                {
                  sliceId: 2
                },
                {
                  sliceId: 2
                },
                {
                  sliceId: 3
                },
                {
                  sliceId: 5
                },
                {
                  sliceId: 1
                },
                {
                  sliceId: 2
                }
              ]
            },
            useNodeSIDs: false,
            useProtectedLinks: 'Protected'
          },
          ero: [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
          ],
          explicitPathName: 'string',
          frrLocalProtection: {
            enabled: true,
            localProtectionType: 'Detour'
          },
          frrProtectingAttributes: {
            localProtectionType: 'NHOP Bypass'
          },
          holdingPriority: 4,
          lastStatusString: 'string',
          lastUpdate: 'string',
          metric: 2,
          preference: 10,
          preferredEro: [
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            }
          ],
          preferredXro: [
            {
              correlated: null
            },
            {
              correlated: null
            },
            {
              correlated: null
            },
            {
              correlated: null
            },
            {
              correlated: null
            },
            {
              correlated: null
            },
            {
              correlated: null
            }
          ],
          protectionCommand: 'manualSwitchToWorking',
          routingStatus: 'Empty',
          rro: [
            null,
            null,
            null,
            null,
            null,
            null,
            null
          ],
          setupPriority: 2,
          srPolicy: {
            color: null,
            endpoint: null,
            discriminator: 8,
            originatorAS: {
              asNumber: null
            },
            originatorAddress: null,
            preference: 100,
            protocolOrigin: 'PCEP'
          },
          timestamp: 7,
          xro: [
            {
              correlated: null
            },
            {
              correlated: null
            },
            {
              correlated: null
            },
            {
              correlated: null
            },
            {
              correlated: null
            },
            {
              correlated: null
            },
            {
              correlated: null
            }
          ]
        },
        constraints: {
          adminStatus: 'Down',
          bandwidth: null,
          bandwidthCalendar: {
            endTime: 'string',
            recurrence: {
              FREQ: 'DAILY',
              INTERVAL: 1,
              UNTIL: 'string'
            },
            startTime: 'string'
          },
          bindingSID: 9,
          calculatedEro: [
            {
              destinationNodes: [
                {
                  nodeIndex: 3,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 9,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 3,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 4,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 2,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 3,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 2,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 10,
                  topoObjectType: 'node'
                }
              ],
              signaling: null,
              sourceNodes: [
                {
                  nodeIndex: 1,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 1,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 3,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 2,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 3,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 9,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 7,
                  topoObjectType: 'node'
                }
              ],
              usedLinks: [
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                }
              ]
            },
            {
              destinationNodes: [
                {
                  nodeIndex: 4,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 5,
                  topoObjectType: 'node'
                }
              ],
              signaling: null,
              sourceNodes: [
                {
                  nodeIndex: 10,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 2,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 2,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 1,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 3,
                  topoObjectType: 'node'
                }
              ],
              usedLinks: [
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                }
              ]
            },
            {
              destinationNodes: [
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 9,
                  topoObjectType: 'node'
                }
              ],
              signaling: null,
              sourceNodes: [
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 10,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 10,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 8,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 9,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 9,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 5,
                  topoObjectType: 'node'
                }
              ],
              usedLinks: [
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                }
              ]
            },
            {
              destinationNodes: [
                {
                  nodeIndex: 8,
                  topoObjectType: 'node'
                }
              ],
              signaling: null,
              sourceNodes: [
                {
                  nodeIndex: 10,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 9,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 8,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 5,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 3,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 2,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 3,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 4,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 4,
                  topoObjectType: 'node'
                }
              ],
              usedLinks: [
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                }
              ]
            },
            {
              destinationNodes: [
                {
                  nodeIndex: 2,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 2,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 5,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 1,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 4,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 5,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 10,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 9,
                  topoObjectType: 'node'
                }
              ],
              signaling: null,
              sourceNodes: [
                {
                  nodeIndex: 2,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 8,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 8,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 3,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 8,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 1,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 9,
                  topoObjectType: 'node'
                }
              ],
              usedLinks: [
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                }
              ]
            },
            {
              destinationNodes: [
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 10,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 2,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 1,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 8,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 2,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 8,
                  topoObjectType: 'node'
                }
              ],
              signaling: null,
              sourceNodes: [
                {
                  nodeIndex: 8,
                  topoObjectType: 'node'
                }
              ],
              usedLinks: [
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                }
              ]
            }
          ],
          color: null,
          comment: 'string',
          computationProfiles: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          controllerStatus: {},
          correlatedRROHopCount: 7,
          design: {
            adminGroups: {
              attributeExclude: 1,
              attributeIncludeAll: 9,
              attributeIncludeAny: 3
            },
            coroutedPairId: 'string',
            diversityGroup: 'string',
            diversityGroupRoutingPriority: 10,
            diversityLevel: 'site',
            maxCost: 3,
            maxDelay: 8,
            maxHop: 4,
            minimumDiversityLevel: 'link',
            reroutingBehavior: 'noRerouting',
            routingMethod: 'default',
            slices: {
              sliceExclude: [
                {
                  sliceId: 9
                },
                {
                  sliceId: 4
                }
              ],
              sliceIncludeAll: [
                {
                  sliceId: 5
                },
                {
                  sliceId: 3
                },
                {
                  sliceId: 9
                },
                {
                  sliceId: 5
                },
                {
                  sliceId: 6
                },
                {
                  sliceId: 1
                },
                {
                  sliceId: 5
                }
              ],
              sliceIncludeAny: [
                {
                  sliceId: 2
                },
                {
                  sliceId: 5
                },
                {
                  sliceId: 7
                },
                {
                  sliceId: 10
                },
                {
                  sliceId: 6
                },
                {
                  sliceId: 10
                },
                {
                  sliceId: 5
                },
                {
                  sliceId: 7
                },
                {
                  sliceId: 10
                }
              ]
            },
            useNodeSIDs: false,
            useProtectedLinks: 'Protected'
          },
          ero: [
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            }
          ],
          flows: [
            {
              group: {
                address: 'string',
                length: 7,
                topoObjectType: 'prefix'
              },
              id: 4,
              source: {
                address: 'string',
                length: 1,
                topoObjectType: 'prefix'
              },
              topoObjectType: 'flow',
              vpn: {
                name: 'string',
                rd: 'string',
                topoObjectType: 'vpnId'
              }
            }
          ],
          holdingPriority: 5,
          lastStatusString: 'string',
          lastUpdate: 'string',
          metric: 1,
          pathDefinitionUsed: 'string',
          pathName: 'string',
          policy: {
            bandwidthSizing: {
              adjustmentThreshold: 10,
              enabled: true,
              maxBandwidth: null,
              minBandwidth: null,
              minVariation: null
            },
            delayBasedMetrics: {
              highDelayMetric: 8,
              highDelayThreshold: 3,
              lowDelayMetric: 4,
              lowDelayThreshold: 1
            }
          },
          preference: 10,
          preferredEro: [
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            }
          ],
          preferredXro: [
            {
              correlated: null
            }
          ],
          requestedProtectionCommand: 'manualSwitchToWorking',
          routingStatus: 'Up',
          setupPriority: 6,
          srPolicy: {
            color: null,
            endpoint: null,
            discriminator: 5,
            originatorAS: {
              asNumber: null
            },
            originatorAddress: null,
            preference: 100,
            protocolOrigin: 'Configuration'
          },
          transportSlice: {
            'ns-connection-id': 9,
            'ns-id': 'string'
          },
          usePenultimateHopAsSignalingAddress: true,
          userProperties: {},
          xro: [
            {
              correlated: null
            },
            {
              correlated: null
            },
            {
              correlated: null
            },
            {
              correlated: null
            }
          ]
        },
        controlType: 'PCC',
        controller: 'NorthStar',
        creationConfigurationMethod: 'PCEP',
        epeProperties: {
          bandwidth: 8,
          externalRates: [
            null,
            null,
            null,
            null
          ],
          internalRate: 3,
          peeringRatePlan: [
            {
              bound: 8,
              rate: 9
            },
            {
              bound: 7,
              rate: 5
            },
            {
              bound: 9,
              rate: 1
            },
            {
              bound: 10,
              rate: 2
            },
            {
              bound: 9,
              rate: 4
            },
            {
              bound: 8,
              rate: 6
            },
            {
              bound: 10,
              rate: 1
            },
            {
              bound: 2,
              rate: 2
            },
            {
              bound: 3,
              rate: 1
            },
            {
              bound: 10,
              rate: 6
            }
          ]
        },
        from: null,
        initiator: 'string',
        lastUpdate: 'string',
        layer: 'IP',
        liveProperties: {
          adminGroups: {
            attributeExclude: 6,
            attributeIncludeAll: 4,
            attributeIncludeAny: 6
          },
          adminStatus: 'Planned',
          bandwidth: null,
          bindingSID: 4,
          ero: [
            null,
            null,
            null,
            null,
            null
          ],
          frrLocalProtection: {
            enabled: true
          },
          holdingPriority: 4,
          metric: 6,
          metricList: [
            {
              bound: false,
              computed: false,
              type: 'MLAdaptationsCount',
              value: 1
            },
            {
              bound: true,
              computed: true,
              type: 'borderNodeCount',
              value: 9
            },
            {
              bound: false,
              computed: false,
              type: 'P2MPPathLossMetric',
              value: 9
            },
            {
              bound: true,
              computed: true,
              type: 'aggregateBandwidthConsumption',
              value: 10
            },
            {
              bound: true,
              computed: true,
              type: 'pathLoss',
              value: 10
            },
            {
              bound: true,
              computed: true,
              type: 'loadOfTheMostLoadedLink',
              value: 8
            },
            {
              bound: false,
              computed: true,
              type: 'pathLoss',
              value: 10
            }
          ],
          operationalStatus: null,
          options: {
            TEPlusPlusId: 4,
            autoBandwidth: true
          },
          pathName: 'string',
          rro: [
            {
              protectionAvailable: 'false',
              protectionInUse: 'false'
            },
            {
              protectionAvailable: 'false',
              protectionInUse: 'false'
            },
            {
              protectionAvailable: 'false',
              protectionInUse: 'false'
            },
            {
              protectionAvailable: 'false',
              protectionInUse: 'false'
            },
            {
              protectionAvailable: 'false',
              protectionInUse: 'false'
            }
          ],
          setupPriority: 10,
          srPolicy: {
            color: null,
            endpoint: null,
            discriminator: 1,
            originatorAS: {
              asNumber: null
            },
            originatorAddress: null,
            preference: 100,
            protocolOrigin: 'Configuration'
          }
        },
        lspIndex: 10,
        name: 'string',
        operationalStatus: null,
        p2mpIndex: 8,
        p2mpName: 'string',
        pathType: 'primary',
        plannedProperties: {
          adminStatus: 'Unknown',
          bandwidth: null,
          bandwidthCalendar: {
            endTime: 'string',
            recurrence: {
              FREQ: 'DAILY',
              INTERVAL: 1,
              UNTIL: 'string'
            },
            startTime: 'string'
          },
          bindingSID: 2,
          calculatedEro: [
            {
              destinationNodes: [
                {
                  nodeIndex: 10,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 7,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 8,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 8,
                  topoObjectType: 'node'
                }
              ],
              signaling: null,
              sourceNodes: [
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 4,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 3,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 10,
                  topoObjectType: 'node'
                }
              ],
              usedLinks: [
                {
                  bandwidth: null
                }
              ]
            },
            {
              destinationNodes: [
                {
                  nodeIndex: 1,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 10,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 10,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 4,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 9,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 2,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 1,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 4,
                  topoObjectType: 'node'
                }
              ],
              signaling: null,
              sourceNodes: [
                {
                  nodeIndex: 4,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 4,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 9,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 7,
                  topoObjectType: 'node'
                }
              ],
              usedLinks: [
                {
                  bandwidth: null
                }
              ]
            },
            {
              destinationNodes: [
                {
                  nodeIndex: 2,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 9,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 9,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 7,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 9,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 5,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 2,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 4,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                }
              ],
              signaling: null,
              sourceNodes: [
                {
                  nodeIndex: 5,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 4,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 7,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 3,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 9,
                  topoObjectType: 'node'
                }
              ],
              usedLinks: [
                {
                  bandwidth: null
                }
              ]
            },
            {
              destinationNodes: [
                {
                  nodeIndex: 8,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 1,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 7,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 7,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 3,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 5,
                  topoObjectType: 'node'
                }
              ],
              signaling: null,
              sourceNodes: [
                {
                  nodeIndex: 1,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 4,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 5,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 10,
                  topoObjectType: 'node'
                }
              ],
              usedLinks: [
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                }
              ]
            },
            {
              destinationNodes: [
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 7,
                  topoObjectType: 'node'
                }
              ],
              signaling: null,
              sourceNodes: [
                {
                  nodeIndex: 9,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 4,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 4,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 1,
                  topoObjectType: 'node'
                }
              ],
              usedLinks: [
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                }
              ]
            },
            {
              destinationNodes: [
                {
                  nodeIndex: 1,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 9,
                  topoObjectType: 'node'
                }
              ],
              signaling: null,
              sourceNodes: [
                {
                  nodeIndex: 5,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 4,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 8,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 2,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 10,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 2,
                  topoObjectType: 'node'
                }
              ],
              usedLinks: [
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                }
              ]
            },
            {
              destinationNodes: [
                {
                  nodeIndex: 4,
                  topoObjectType: 'node'
                }
              ],
              signaling: null,
              sourceNodes: [
                {
                  nodeIndex: 3,
                  topoObjectType: 'node'
                }
              ],
              usedLinks: [
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                }
              ]
            },
            {
              destinationNodes: [
                {
                  nodeIndex: 6,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 3,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 2,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 1,
                  topoObjectType: 'node'
                }
              ],
              signaling: null,
              sourceNodes: [
                {
                  nodeIndex: 8,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 10,
                  topoObjectType: 'node'
                },
                {
                  nodeIndex: 8,
                  topoObjectType: 'node'
                }
              ],
              usedLinks: [
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                },
                {
                  bandwidth: null
                }
              ]
            }
          ],
          color: null,
          comment: 'string',
          computationProfiles: [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
            'string'
          ],
          controllerStatus: {},
          correlatedRROHopCount: 3,
          design: {
            adminGroups: {
              attributeExclude: 4,
              attributeIncludeAll: 5,
              attributeIncludeAny: 7
            },
            coroutedPairId: 'string',
            diversityGroup: 'string',
            diversityGroupRoutingPriority: 6,
            diversityLevel: 'site',
            maxCost: 8,
            maxDelay: 10,
            maxHop: 9,
            minimumDiversityLevel: 'srlg',
            reroutingBehavior: 'noRerouting',
            routingMethod: 'default',
            slices: {
              sliceExclude: [
                {
                  sliceId: 7
                },
                {
                  sliceId: 8
                },
                {
                  sliceId: 2
                },
                {
                  sliceId: 3
                },
                {
                  sliceId: 2
                },
                {
                  sliceId: 2
                },
                {
                  sliceId: 3
                }
              ],
              sliceIncludeAll: [
                {
                  sliceId: 7
                },
                {
                  sliceId: 8
                },
                {
                  sliceId: 8
                },
                {
                  sliceId: 9
                },
                {
                  sliceId: 5
                },
                {
                  sliceId: 3
                },
                {
                  sliceId: 9
                },
                {
                  sliceId: 2
                },
                {
                  sliceId: 2
                },
                {
                  sliceId: 2
                }
              ],
              sliceIncludeAny: [
                {
                  sliceId: 9
                },
                {
                  sliceId: 10
                },
                {
                  sliceId: 6
                },
                {
                  sliceId: 1
                },
                {
                  sliceId: 1
                },
                {
                  sliceId: 7
                },
                {
                  sliceId: 5
                },
                {
                  sliceId: 10
                },
                {
                  sliceId: 5
                },
                {
                  sliceId: 10
                }
              ]
            },
            useNodeSIDs: true,
            useProtectedLinks: 'Protected'
          },
          ero: [
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            }
          ],
          flows: [
            {
              group: {
                address: 'string',
                length: 4,
                topoObjectType: 'prefix'
              },
              id: 9,
              source: {
                address: 'string',
                length: 3,
                topoObjectType: 'prefix'
              },
              topoObjectType: 'flow',
              vpn: {
                name: 'string',
                rd: 'string',
                topoObjectType: 'vpnId'
              }
            },
            {
              group: {
                address: 'string',
                length: 4,
                topoObjectType: 'prefix'
              },
              id: 1,
              source: {
                address: 'string',
                length: 2,
                topoObjectType: 'prefix'
              },
              topoObjectType: 'flow',
              vpn: {
                name: 'string',
                rd: 'string',
                topoObjectType: 'vpnId'
              }
            },
            {
              group: {
                address: 'string',
                length: 3,
                topoObjectType: 'prefix'
              },
              id: 8,
              source: {
                address: 'string',
                length: 10,
                topoObjectType: 'prefix'
              },
              topoObjectType: 'flow',
              vpn: {
                name: 'string',
                rd: 'string',
                topoObjectType: 'vpnId'
              }
            },
            {
              group: {
                address: 'string',
                length: 1,
                topoObjectType: 'prefix'
              },
              id: 2,
              source: {
                address: 'string',
                length: 3,
                topoObjectType: 'prefix'
              },
              topoObjectType: 'flow',
              vpn: {
                name: 'string',
                rd: 'string',
                topoObjectType: 'vpnId'
              }
            }
          ],
          holdingPriority: 4,
          lastStatusString: 'string',
          lastUpdate: 'string',
          metric: 8,
          pathDefinitionUsed: 'string',
          pathName: 'string',
          policy: {
            bandwidthSizing: {
              adjustmentThreshold: 10,
              enabled: false,
              maxBandwidth: null,
              minBandwidth: null,
              minVariation: null
            },
            delayBasedMetrics: {
              highDelayMetric: 8,
              highDelayThreshold: 2,
              lowDelayMetric: 1,
              lowDelayThreshold: 1
            }
          },
          preference: 10,
          preferredEro: [
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            },
            {
              correlated: null,
              loose: 'false'
            }
          ],
          preferredXro: [
            {
              correlated: null
            },
            {
              correlated: null
            },
            {
              correlated: null
            },
            {
              correlated: null
            }
          ],
          requestedProtectionCommand: 'manualSwitchToProtecting',
          routingStatus: 'Empty',
          setupPriority: 10,
          srPolicy: {
            color: null,
            endpoint: null,
            discriminator: 9,
            originatorAS: {
              asNumber: null
            },
            originatorAddress: null,
            preference: 100,
            protocolOrigin: 'PCEP'
          },
          transportSlice: {
            'ns-connection-id': 3,
            'ns-id': 'string'
          },
          usePenultimateHopAsSignalingAddress: true,
          userProperties: {},
          xro: [
            {
              correlated: null
            },
            {
              correlated: null
            },
            {
              correlated: null
            },
            {
              correlated: null
            },
            {
              correlated: null
            }
          ]
        },
        provisioningType: 'RSVP',
        teContainer: null,
        to: null,
        toSignalingAddress: null,
        tunnelId: 1
      }
    ];
    describe('#putTopologyV2TopologyIdTeLspsBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTopologyV2TopologyIdTeLspsBulk(555, topologyPutTopologyV2TopologyIdTeLspsBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'putTopologyV2TopologyIdTeLspsBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPatchTopologyV2TopologyIdTeLspsBulkBodyParam = [
      {
        lspIndex: 5,
        patch: [
          {
            path: 'string'
          },
          {
            path: 'string'
          }
        ]
      }
    ];
    describe('#patchTopologyV2TopologyIdTeLspsBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTopologyV2TopologyIdTeLspsBulk(555, topologyPatchTopologyV2TopologyIdTeLspsBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'patchTopologyV2TopologyIdTeLspsBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdTeLspsBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyV2TopologyIdTeLspsBulk(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyV2TopologyIdTeLspsBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdTeLspsLspIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdTeLspsLspIndex(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdTeLspsLspIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPutTopologyV2TopologyIdTeLspsLspIndexBodyParam = {};
    describe('#putTopologyV2TopologyIdTeLspsLspIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTopologyV2TopologyIdTeLspsLspIndex(555, 555, topologyPutTopologyV2TopologyIdTeLspsLspIndexBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'putTopologyV2TopologyIdTeLspsLspIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPatchTopologyV2TopologyIdTeLspsLspIndexBodyParam = [
      {
        path: 'string'
      }
    ];
    describe('#patchTopologyV2TopologyIdTeLspsLspIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTopologyV2TopologyIdTeLspsLspIndex(555, 555, topologyPatchTopologyV2TopologyIdTeLspsLspIndexBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'patchTopologyV2TopologyIdTeLspsLspIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdTeLspsLspIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyV2TopologyIdTeLspsLspIndex(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyV2TopologyIdTeLspsLspIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdTeLspsLspIndexHistory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdTeLspsLspIndexHistory(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdTeLspsLspIndexHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdDemands - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdDemands(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdDemands', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPostTopologyV2TopologyIdDemandsBodyParam = {};
    describe('#postTopologyV2TopologyIdDemands - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTopologyV2TopologyIdDemands(555, topologyPostTopologyV2TopologyIdDemandsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'postTopologyV2TopologyIdDemands', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdDemandsStream - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdDemandsStream(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdDemandsStream', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdDemandsDemandIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdDemandsDemandIndex(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdDemandsDemandIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPutTopologyV2TopologyIdDemandsDemandIndexBodyParam = {};
    describe('#putTopologyV2TopologyIdDemandsDemandIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTopologyV2TopologyIdDemandsDemandIndex(555, 555, topologyPutTopologyV2TopologyIdDemandsDemandIndexBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'putTopologyV2TopologyIdDemandsDemandIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPatchTopologyV2TopologyIdDemandsDemandIndexBodyParam = [
      {
        path: 'string'
      }
    ];
    describe('#patchTopologyV2TopologyIdDemandsDemandIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTopologyV2TopologyIdDemandsDemandIndex(555, 555, topologyPatchTopologyV2TopologyIdDemandsDemandIndexBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'patchTopologyV2TopologyIdDemandsDemandIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdDemandsDemandIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyV2TopologyIdDemandsDemandIndex(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyV2TopologyIdDemandsDemandIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPostTopologyV2TopologyIdDemandsBulkBodyParam = [
      null
    ];
    describe('#postTopologyV2TopologyIdDemandsBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTopologyV2TopologyIdDemandsBulk(555, topologyPostTopologyV2TopologyIdDemandsBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'postTopologyV2TopologyIdDemandsBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPutTopologyV2TopologyIdDemandsBulkBodyParam = [
      null
    ];
    describe('#putTopologyV2TopologyIdDemandsBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTopologyV2TopologyIdDemandsBulk(555, topologyPutTopologyV2TopologyIdDemandsBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'putTopologyV2TopologyIdDemandsBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPatchTopologyV2TopologyIdDemandsBulkBodyParam = [
      {
        demandIndex: 1,
        patch: [
          {
            path: 'string'
          },
          {
            path: 'string'
          },
          {
            path: 'string'
          },
          {
            path: 'string'
          },
          {
            path: 'string'
          }
        ]
      }
    ];
    describe('#patchTopologyV2TopologyIdDemandsBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTopologyV2TopologyIdDemandsBulk(555, topologyPatchTopologyV2TopologyIdDemandsBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'patchTopologyV2TopologyIdDemandsBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdDemandsBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyV2TopologyIdDemandsBulk(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyV2TopologyIdDemandsBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdP2mp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdP2mp(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdP2mp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPostTopologyV2TopologyIdP2mpBodyParam = {};
    describe('#postTopologyV2TopologyIdP2mp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTopologyV2TopologyIdP2mp(555, topologyPostTopologyV2TopologyIdP2mpBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'postTopologyV2TopologyIdP2mp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdP2mpStream - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdP2mpStream(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdP2mpStream', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdP2mpBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyV2TopologyIdP2mpBulk(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyV2TopologyIdP2mpBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdP2mpP2mpGroupIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdP2mpP2mpGroupIndex(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdP2mpP2mpGroupIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPutTopologyV2TopologyIdP2mpP2mpGroupIndexBodyParam = {
      from: null,
      lsps: [
        {
          from: null,
          lspIndex: 8,
          plannedProperties: null,
          to: null
        }
      ],
      name: 'string',
      p2mpGroupIndex: 6,
      p2mpIndex: 5,
      plannedProperties: null,
      topoObjectType: 'p2mpGroup'
    };
    describe('#putTopologyV2TopologyIdP2mpP2mpGroupIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTopologyV2TopologyIdP2mpP2mpGroupIndex(555, 555, topologyPutTopologyV2TopologyIdP2mpP2mpGroupIndexBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'putTopologyV2TopologyIdP2mpP2mpGroupIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdP2mpP2mpGroupIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyV2TopologyIdP2mpP2mpGroupIndex(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyV2TopologyIdP2mpP2mpGroupIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPatchTopologyV2TopologyIdP2mpP2mpGroupIndexBodyParam = [
      {
        path: 'string'
      }
    ];
    describe('#patchTopologyV2TopologyIdP2mpP2mpGroupIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTopologyV2TopologyIdP2mpP2mpGroupIndex(555, 555, topologyPatchTopologyV2TopologyIdP2mpP2mpGroupIndexBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'patchTopologyV2TopologyIdP2mpP2mpGroupIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPostTopologyV2TopologyIdP2mpP2mpGroupIndexBodyParam = {};
    describe('#postTopologyV2TopologyIdP2mpP2mpGroupIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTopologyV2TopologyIdP2mpP2mpGroupIndex(555, 555, topologyPostTopologyV2TopologyIdP2mpP2mpGroupIndexBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'postTopologyV2TopologyIdP2mpP2mpGroupIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex(555, 555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex(555, 555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdP2mpP2mpGroupIndexBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyV2TopologyIdP2mpP2mpGroupIndexBulk(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyV2TopologyIdP2mpP2mpGroupIndexBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdVrfs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdVrfs(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdVrfs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdVrfsNodeIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdVrfsNodeIndex(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdVrfsNodeIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdMaintenances - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdMaintenances(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdMaintenances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPostTopologyV2TopologyIdMaintenancesBodyParam = {
      elements: [
        null
      ],
      endTime: 'string',
      name: 'string',
      startTime: 'string',
      topoObjectType: 'maintenance',
      topologyIndex: null
    };
    describe('#postTopologyV2TopologyIdMaintenances - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTopologyV2TopologyIdMaintenances(555, topologyPostTopologyV2TopologyIdMaintenancesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'postTopologyV2TopologyIdMaintenances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdMaintenancesStream - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdMaintenancesStream(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdMaintenancesStream', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdMaintenancesMaintenanceIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdMaintenancesMaintenanceIndex(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdMaintenancesMaintenanceIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPutTopologyV2TopologyIdMaintenancesMaintenanceIndexBodyParam = {
      elements: [
        null
      ],
      endTime: 'string',
      maintenanceIndex: 5,
      startTime: 'string',
      topoObjectType: 'maintenance',
      topologyIndex: null
    };
    describe('#putTopologyV2TopologyIdMaintenancesMaintenanceIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTopologyV2TopologyIdMaintenancesMaintenanceIndex(555, 555, topologyPutTopologyV2TopologyIdMaintenancesMaintenanceIndexBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'putTopologyV2TopologyIdMaintenancesMaintenanceIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdMaintenancesMaintenanceIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyV2TopologyIdMaintenancesMaintenanceIndex(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyV2TopologyIdMaintenancesMaintenanceIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdFacilities - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdFacilities(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdFacilities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPostTopologyV2TopologyIdFacilitiesBodyParam = {
      name: 'string',
      topoObjectType: 'facility',
      topologyIndex: null
    };
    describe('#postTopologyV2TopologyIdFacilities - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTopologyV2TopologyIdFacilities(555, topologyPostTopologyV2TopologyIdFacilitiesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'postTopologyV2TopologyIdFacilities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdFacilitiesStream - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdFacilitiesStream(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdFacilitiesStream', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdFacilitiesFacilityIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdFacilitiesFacilityIndex(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdFacilitiesFacilityIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPutTopologyV2TopologyIdFacilitiesFacilityIndexBodyParam = {};
    describe('#putTopologyV2TopologyIdFacilitiesFacilityIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTopologyV2TopologyIdFacilitiesFacilityIndex(555, 555, topologyPutTopologyV2TopologyIdFacilitiesFacilityIndexBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'putTopologyV2TopologyIdFacilitiesFacilityIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdFacilitiesFacilityIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyV2TopologyIdFacilitiesFacilityIndex(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyV2TopologyIdFacilitiesFacilityIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdTeContainers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdTeContainers(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdTeContainers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPostTopologyV2TopologyIdTeContainersBodyParam = {
      from: null,
      maximumLSPCount: 64,
      mergingBandwidth: null,
      minimumLSPCount: 1,
      name: 'string',
      splittingBandwidth: null,
      to: null
    };
    describe('#postTopologyV2TopologyIdTeContainers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTopologyV2TopologyIdTeContainers(555, topologyPostTopologyV2TopologyIdTeContainersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'postTopologyV2TopologyIdTeContainers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdTeContainersStream - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdTeContainersStream(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdTeContainersStream', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdTeContainersContainerIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdTeContainersContainerIndex(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdTeContainersContainerIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPutTopologyV2TopologyIdTeContainersContainerIndexBodyParam = {
      containerIndex: 9,
      from: null,
      maximumLSPCount: 64,
      mergingBandwidth: null,
      minimumLSPCount: 1,
      name: 'string',
      splittingBandwidth: null,
      to: null
    };
    describe('#putTopologyV2TopologyIdTeContainersContainerIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTopologyV2TopologyIdTeContainersContainerIndex(555, 555, topologyPutTopologyV2TopologyIdTeContainersContainerIndexBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'putTopologyV2TopologyIdTeContainersContainerIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPatchTopologyV2TopologyIdTeContainersContainerIndexBodyParam = [
      {
        path: 'string'
      }
    ];
    describe('#patchTopologyV2TopologyIdTeContainersContainerIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTopologyV2TopologyIdTeContainersContainerIndex(555, 555, topologyPatchTopologyV2TopologyIdTeContainersContainerIndexBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'patchTopologyV2TopologyIdTeContainersContainerIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdTeContainersContainerIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyV2TopologyIdTeContainersContainerIndex(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyV2TopologyIdTeContainersContainerIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPostTopologyV2TopologyIdTeContainersBulkBodyParam = {};
    describe('#postTopologyV2TopologyIdTeContainersBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTopologyV2TopologyIdTeContainersBulk(555, topologyPostTopologyV2TopologyIdTeContainersBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'postTopologyV2TopologyIdTeContainersBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPutTopologyV2TopologyIdTeContainersBulkBodyParam = [
      {
        containerIndex: 2,
        creationConfigurationMethod: 'PCEP',
        from: null,
        maximumLSPBandwidth: null,
        maximumLSPCount: 64,
        mergingBandwidth: null,
        minimumLSPBandwidth: null,
        minimumLSPCount: 1,
        name: 'string',
        provisioningType: 'RSVP',
        splittingBandwidth: null,
        subLSPProperties: {
          adminStatus: 'Shutdown',
          bindingSID: 2,
          color: null,
          comment: 'string',
          design: {
            adminGroups: {
              attributeExclude: 10,
              attributeIncludeAll: 8,
              attributeIncludeAny: 4
            },
            coroutedPairId: 'string',
            diversityGroup: 'string',
            diversityGroupRoutingPriority: 9,
            diversityLevel: 'site',
            maxCost: 3,
            maxDelay: 4,
            maxHop: 5,
            minimumDiversityLevel: 'srlg',
            reroutingBehavior: 'noRerouting',
            routingMethod: 'default',
            slices: {
              sliceExclude: [
                {
                  sliceId: 6
                },
                {
                  sliceId: 8
                },
                {
                  sliceId: 9
                },
                {
                  sliceId: 6
                },
                {
                  sliceId: 9
                },
                {
                  sliceId: 4
                },
                {
                  sliceId: 1
                }
              ],
              sliceIncludeAll: [
                {
                  sliceId: 8
                },
                {
                  sliceId: 6
                },
                {
                  sliceId: 10
                },
                {
                  sliceId: 10
                },
                {
                  sliceId: 3
                },
                {
                  sliceId: 1
                },
                {
                  sliceId: 1
                }
              ],
              sliceIncludeAny: [
                {
                  sliceId: 4
                },
                {
                  sliceId: 1
                },
                {
                  sliceId: 3
                },
                {
                  sliceId: 8
                },
                {
                  sliceId: 3
                }
              ]
            },
            useNodeSIDs: false,
            useProtectedLinks: 'Protected'
          },
          ero: [
            {
              loose: 'false'
            },
            {
              loose: 'false'
            }
          ],
          holdingPriority: 3,
          metric: 9,
          policy: {
            bandwidthSizing: {
              adjustmentThreshold: 10,
              enabled: false,
              maxBandwidth: null,
              minBandwidth: null,
              minVariation: null
            },
            delayBasedMetrics: {
              highDelayMetric: 10,
              highDelayThreshold: 6,
              lowDelayMetric: 5,
              lowDelayThreshold: 3
            }
          },
          preferredEro: [
            {
              loose: 'false'
            }
          ],
          preferredXro: [
            {
              loose: 'false'
            },
            {
              loose: 'false'
            },
            {
              loose: 'false'
            }
          ],
          setupPriority: 2,
          srPolicy: {
            color: null,
            endpoint: null,
            discriminator: 5,
            originatorAS: {
              asNumber: null
            },
            originatorAddress: null,
            preference: 100,
            protocolOrigin: 'PCEP'
          },
          userProperties: {},
          xro: [
            {
              loose: 'false'
            },
            {
              loose: 'false'
            }
          ]
        },
        subLSPs: [
          null,
          null
        ],
        to: null
      }
    ];
    describe('#putTopologyV2TopologyIdTeContainersBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTopologyV2TopologyIdTeContainersBulk(555, topologyPutTopologyV2TopologyIdTeContainersBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'putTopologyV2TopologyIdTeContainersBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPatchTopologyV2TopologyIdTeContainersBulkBodyParam = [
      {
        containerIndex: 3,
        patch: [
          {
            path: 'string'
          },
          {
            path: 'string'
          },
          {
            path: 'string'
          },
          {
            path: 'string'
          },
          {
            path: 'string'
          }
        ]
      }
    ];
    describe('#patchTopologyV2TopologyIdTeContainersBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTopologyV2TopologyIdTeContainersBulk(555, topologyPatchTopologyV2TopologyIdTeContainersBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'patchTopologyV2TopologyIdTeContainersBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdTeContainersBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyV2TopologyIdTeContainersBulk(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyV2TopologyIdTeContainersBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdRoutes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdRoutes(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdRoutes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdRoutesNodeIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdRoutesNodeIndex(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdRoutesNodeIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPostTopologyV2TopologyIdPathComputationBodyParam = {
      requests: [
        {
          bandwidth: null,
          design: {
            maxDelay: 5,
            maxHop: 9
          },
          exclusions: [
            null
          ],
          from: null,
          name: 'string',
          plannedProperties: {
            adminStatus: 'Unknown',
            bandwidth: null,
            bindingSID: 8,
            calculatedEro: [
              {
                destinationNodes: [
                  {
                    nodeIndex: 3,
                    topoObjectType: 'node'
                  }
                ],
                signaling: null,
                sourceNodes: [
                  {
                    nodeIndex: 4,
                    topoObjectType: 'node'
                  }
                ],
                usedLinks: [
                  {
                    bandwidth: null
                  }
                ]
              }
            ],
            comment: 'string',
            computationProfiles: [
              'string'
            ],
            design: {
              adminGroups: {
                attributeExclude: 1,
                attributeIncludeAll: 9,
                attributeIncludeAny: 7
              },
              coroutedPairId: 'string',
              diversityGroup: 'string',
              diversityGroupRoutingPriority: 8,
              diversityLevel: 'srlg',
              maxCost: 10,
              maxDelay: 9,
              maxHop: 7,
              minimumDiversityLevel: 'site',
              reroutingBehavior: 'noRerouting',
              routingMethod: 'default',
              slices: {
                sliceExclude: [
                  {
                    sliceId: 3
                  }
                ],
                sliceIncludeAll: [
                  {
                    sliceId: 10
                  }
                ],
                sliceIncludeAny: [
                  {
                    sliceId: 6
                  }
                ]
              },
              useNodeSIDs: true,
              useProtectedLinks: 'Protected'
            },
            ero: [
              {
                correlated: null,
                loose: 'false'
              }
            ],
            flows: [
              {
                group: {
                  address: 'string',
                  length: 3,
                  topoObjectType: 'prefix'
                },
                id: 2,
                source: {
                  address: 'string',
                  length: 5,
                  topoObjectType: 'prefix'
                },
                topoObjectType: 'flow',
                vpn: {
                  name: 'string',
                  rd: 'string',
                  topoObjectType: 'vpnId'
                }
              }
            ],
            holdingPriority: 3,
            metric: 2,
            pathName: 'string',
            policy: {
              bandwidthSizing: {
                adjustmentThreshold: 10,
                enabled: true,
                maxBandwidth: null,
                minBandwidth: null,
                minVariation: null
              },
              delayBasedMetrics: {
                highDelayMetric: 3,
                highDelayThreshold: 10,
                lowDelayMetric: 7,
                lowDelayThreshold: 5
              }
            },
            preference: 10,
            preferredEro: [
              {
                correlated: null,
                loose: 'false'
              }
            ],
            preferredXro: [
              {
                correlated: null
              }
            ],
            setupPriority: 9,
            transportSlice: {
              'ns-connection-id': 9,
              'ns-id': 'string'
            },
            userProperties: {},
            xro: [
              {
                correlated: null
              }
            ]
          },
          provisioningType: 'RSVP',
          to: null
        }
      ]
    };
    describe('#postTopologyV2TopologyIdPathComputation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTopologyV2TopologyIdPathComputation(555, topologyPostTopologyV2TopologyIdPathComputationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'postTopologyV2TopologyIdPathComputation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPostTopologyV2RpcOptimizeBodyParam = {
      elements: [
        {
          index: 6,
          topoObjectType: 'link'
        }
      ]
    };
    describe('#postTopologyV2RpcOptimize - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTopologyV2RpcOptimize(topologyPostTopologyV2RpcOptimizeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'postTopologyV2RpcOptimize', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2RpcOptimize - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2RpcOptimize((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2RpcOptimize', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPostTopologyV2RpcSimulationBodyParam = {
      detailedReport: true,
      elements: [
        null
      ],
      endTime: 'string',
      startTime: 'string',
      topologyIndex: null
    };
    describe('#postTopologyV2RpcSimulation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTopologyV2RpcSimulation(topologyPostTopologyV2RpcSimulationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'postTopologyV2RpcSimulation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2RpcSimulation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2RpcSimulation((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2RpcSimulation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2RpcSimulationUuid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2RpcSimulationUuid('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2RpcSimulationUuid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2RpcSimulationUuidReportName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2RpcSimulationUuidReportName('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2RpcSimulationUuidReportName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPostTopologyV2RpcDiverseTreeDesignBodyParam = {
      P2MPGroup: null,
      creationConfigurationMethod: 'NETCONF',
      diverseP2MPGroup: null,
      ignoreExistingPath: true,
      maxIterations: 10,
      networkTime: 'string',
      sites: [
        {
          nodes: [
            {
              nodeIndex: 7
            }
          ],
          site: 'string'
        }
      ],
      useResultAsEro: true
    };
    describe('#postTopologyV2RpcDiverseTreeDesign - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTopologyV2RpcDiverseTreeDesign(topologyPostTopologyV2RpcDiverseTreeDesignBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'postTopologyV2RpcDiverseTreeDesign', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2RpcDiverseTreeDesign - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2RpcDiverseTreeDesign((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2RpcDiverseTreeDesign', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2RpcDiverseTreeDesignUuid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2RpcDiverseTreeDesignUuid('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2RpcDiverseTreeDesignUuid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchedulerV2Tasklist - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSchedulerV2Tasklist((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scheduler', 'getSchedulerV2Tasklist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchedulerV2Tasks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSchedulerV2Tasks((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scheduler', 'getSchedulerV2Tasks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulerPostSchedulerV2AddtaskBodyParam = {
      scheduleType: 4,
      taskType: 'string'
    };
    describe('#postSchedulerV2Addtask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSchedulerV2Addtask(schedulerPostSchedulerV2AddtaskBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scheduler', 'postSchedulerV2Addtask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulerPostSchedulerV2UpdatetaskBodyParam = {
      scheduleType: 0,
      taskType: 'string'
    };
    describe('#postSchedulerV2Updatetask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSchedulerV2Updatetask(schedulerPostSchedulerV2UpdatetaskBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scheduler', 'postSchedulerV2Updatetask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulerPostSchedulerV2RemoveaddtaskBodyParam = {
      scheduleType: 4,
      taskType: 'string'
    };
    describe('#postSchedulerV2Removeaddtask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSchedulerV2Removeaddtask(schedulerPostSchedulerV2RemoveaddtaskBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scheduler', 'postSchedulerV2Removeaddtask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulerPostSchedulerV2DeletetaskBodyParam = [
      'string'
    ];
    describe('#postSchedulerV2Deletetask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSchedulerV2Deletetask(schedulerPostSchedulerV2DeletetaskBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scheduler', 'postSchedulerV2Deletetask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchedulerV2TaskshistoryTaskId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSchedulerV2TaskshistoryTaskId('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scheduler', 'getSchedulerV2TaskshistoryTaskId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchedulerV2TaskstatusTaskId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSchedulerV2TaskstatusTaskId('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scheduler', 'getSchedulerV2TaskstatusTaskId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComponentConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getComponentConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Config', 'getComponentConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configPostConfigBodyParam = {};
    describe('#postConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postConfig(configPostConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Config', 'postConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configPatchConfigBodyParam = {};
    describe('#patchConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchConfig(configPatchConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Config', 'patchConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Config', 'deleteConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportControllersV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTransportControllersV2((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TransportControllers', 'getTransportControllersV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const transportControllersPostTransportControllersV2BodyParam = {
      name: 'string',
      profileName: 'string',
      topoObjectType: 'transportController'
    };
    describe('#postTransportControllersV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTransportControllersV2(transportControllersPostTransportControllersV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TransportControllers', 'postTransportControllersV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportControllersV2TransportControllerIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTransportControllersV2TransportControllerIndex(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TransportControllers', 'getTransportControllersV2TransportControllerIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const transportControllersPutTransportControllersV2TransportControllerIndexBodyParam = {};
    describe('#putTransportControllersV2TransportControllerIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTransportControllersV2TransportControllerIndex(555, transportControllersPutTransportControllersV2TransportControllerIndexBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TransportControllers', 'putTransportControllersV2TransportControllerIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransportControllersV2TransportControllerIndex - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTransportControllersV2TransportControllerIndex(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TransportControllers', 'deleteTransportControllersV2TransportControllerIndex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportControllerGroupsV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTransportControllerGroupsV2((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TransportControllerGroups', 'getTransportControllerGroupsV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const transportControllerGroupsPostTransportControllerGroupsV2BodyParam = [
      {
        accessMethod: 'HTTP',
        authMethod: 'BASIC',
        authUrl: 'string',
        hostName: 'string',
        httpPort: null,
        id: 'string',
        ipAddr: 'string',
        login: 'string',
        name: 'string',
        newIpAddr: 'string',
        passwd: 'string',
        retry: 2,
        timeout: 7,
        tokenExpiryTime: '3600'
      }
    ];
    describe('#postTransportControllerGroupsV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTransportControllerGroupsV2(transportControllerGroupsPostTransportControllerGroupsV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TransportControllerGroups', 'postTransportControllerGroupsV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransportControllerGroupsV2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTransportControllerGroupsV2((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TransportControllerGroups', 'deleteTransportControllerGroupsV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportControllerGroupsV2TransportControllerGroupName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTransportControllerGroupsV2TransportControllerGroupName('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TransportControllerGroups', 'getTransportControllerGroupsV2TransportControllerGroupName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const transportControllerGroupsPutTransportControllerGroupsV2TransportControllerGroupNameBodyParam = [
      {
        accessMethod: 'HTTP',
        authMethod: 'BEARER',
        authUrl: 'string',
        hostName: 'string',
        httpPort: null,
        id: 'string',
        ipAddr: 'string',
        login: 'string',
        name: 'string',
        newIpAddr: 'string',
        passwd: 'string',
        retry: 9,
        timeout: 6,
        tokenExpiryTime: '3600'
      }
    ];
    describe('#putTransportControllerGroupsV2TransportControllerGroupName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTransportControllerGroupsV2TransportControllerGroupName('fakedata', transportControllerGroupsPutTransportControllerGroupsV2TransportControllerGroupNameBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TransportControllerGroups', 'putTransportControllerGroupsV2TransportControllerGroupName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const transportControllerGroupsPostTransportControllerGroupsV2TransportControllerGroupNameBodyParam = [
      {
        accessMethod: 'HTTP',
        authMethod: 'BEARER',
        authUrl: 'string',
        hostName: 'string',
        httpPort: null,
        id: 'string',
        ipAddr: 'string',
        login: 'string',
        name: 'string',
        newIpAddr: 'string',
        passwd: 'string',
        retry: 9,
        timeout: 7,
        tokenExpiryTime: '3600'
      }
    ];
    describe('#postTransportControllerGroupsV2TransportControllerGroupName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTransportControllerGroupsV2TransportControllerGroupName('fakedata', transportControllerGroupsPostTransportControllerGroupsV2TransportControllerGroupNameBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TransportControllerGroups', 'postTransportControllerGroupsV2TransportControllerGroupName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransportControllerGroupsV2TransportControllerGroupName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTransportControllerGroupsV2TransportControllerGroupName('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TransportControllerGroups', 'deleteTransportControllerGroupsV2TransportControllerGroupName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetconfV2Profiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetconfV2Profiles((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netconf', 'getNetconfV2Profiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netconfPostNetconfV2ProfilesBodyParam = [
      {
        MD5String: 'string',
        accessMethod: 'ssh',
        city: 'string',
        country: 'string',
        deviceGroup: 'string',
        enLevel: 2,
        getTopology: 3,
        groupName: 'string',
        hostName: 'string',
        id: 'string',
        ipAddr: 'string',
        ipv6Addr: 'string',
        login: 'string',
        model: 'string',
        netconfBulkCommit: true,
        netconfEnable: true,
        netconfRetry: 5,
        networkname: 'string',
        os: 'string',
        osVersion: 'string',
        pLogin: 'string',
        pPasswd: 'string',
        passwd: 'string',
        pcepIp: 'string',
        pcepVersion: '0',
        prpdEnable: false,
        prpdIp: null,
        prpdPort: 4,
        prpdSSL: true,
        retry: 2,
        routerType: 'HUAWEI',
        secondaryIp: null,
        snmpGetCommunity: 'string',
        snmpPortNumber: 4,
        snmpRetry: 5,
        snmpSetCommunity: 'string',
        snmpTimeout: 8,
        snmpVersion: 'V2C',
        sshCommand: 'string',
        telnetPort: 23,
        timeout: 300,
        userProperties: 'string',
        v3Auth: 'string',
        v3AuthPasswd: 'string',
        v3ContextEngine: 'string',
        v3ContextName: 'string',
        v3Privacy: 'string',
        v3PrivacyPasswd: 'string',
        v3UserName: 'string'
      }
    ];
    describe('#postNetconfV2Profiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postNetconfV2Profiles(netconfPostNetconfV2ProfilesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netconf', 'postNetconfV2Profiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netconfPutNetconfV2ProfilesBodyParam = [
      {
        MD5String: 'string',
        accessMethod: 'ssh',
        city: 'string',
        country: 'string',
        deviceGroup: 'string',
        enLevel: 3,
        getTopology: 6,
        groupName: 'string',
        hostName: 'string',
        id: 'string',
        ipAddr: 'string',
        ipv6Addr: 'string',
        login: 'string',
        model: 'string',
        netconfBulkCommit: false,
        netconfEnable: true,
        netconfRetry: 4,
        networkname: 'string',
        os: 'string',
        osVersion: 'string',
        pLogin: 'string',
        pPasswd: 'string',
        passwd: 'string',
        pcepIp: 'string',
        pcepVersion: '0',
        prpdEnable: false,
        prpdIp: null,
        prpdPort: 4,
        prpdSSL: true,
        retry: 4,
        routerType: 'JUNIPER',
        secondaryIp: null,
        snmpGetCommunity: 'string',
        snmpPortNumber: 4,
        snmpRetry: 4,
        snmpSetCommunity: 'string',
        snmpTimeout: 10,
        snmpVersion: 'V2C',
        sshCommand: 'string',
        telnetPort: 23,
        timeout: 300,
        userProperties: 'string',
        v3Auth: 'string',
        v3AuthPasswd: 'string',
        v3ContextEngine: 'string',
        v3ContextName: 'string',
        v3Privacy: 'string',
        v3PrivacyPasswd: 'string',
        v3UserName: 'string'
      }
    ];
    describe('#putNetconfV2Profiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putNetconfV2Profiles(netconfPutNetconfV2ProfilesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netconf', 'putNetconfV2Profiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetconfV2Profiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetconfV2Profiles((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netconf', 'deleteNetconfV2Profiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netconfPostNetconfV2CollectionLiveNetworkBodyParam = {
      idList: [
        'string'
      ],
      opts: {
        concurrentJob: 5,
        runParsing: true,
        useMgmt: false
      }
    };
    describe('#postNetconfV2CollectionLiveNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postNetconfV2CollectionLiveNetwork(netconfPostNetconfV2CollectionLiveNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netconf', 'postNetconfV2CollectionLiveNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetconfV2CollectionCollectionJobId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetconfV2CollectionCollectionJobId(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netconf', 'getNetconfV2CollectionCollectionJobId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdIpePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdIpePolicy(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdIpePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPostTopologyV2TopologyIdIpePolicyBodyParam = [
      {}
    ];
    describe('#postTopologyV2TopologyIdIpePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTopologyV2TopologyIdIpePolicy(555, topologyPostTopologyV2TopologyIdIpePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'postTopologyV2TopologyIdIpePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyPutTopologyV2TopologyIdIpePolicyBodyParam = [
      {
        asPath: {
          regex: 'string'
        },
        id: 9,
        med: 6,
        neighbor: null,
        node: {
          AutonomousSystem: {
            asNumber: null
          },
          activeAssurance: {
            agentPath: 'string'
          },
          comment: 'string',
          configurationDataSource: {
            latest: {
              href: 'string',
              rel: 'string',
              schema: null,
              type: 'string'
            }
          },
          design: {
            delay: 7,
            simulation: {
              canFail: true
            },
            trafficRerouteThreshold: 4,
            userCost: 1
          },
          deviceUuid: 'string',
          epeProperties: {
            bandwidth: 4,
            externalRates: [
              null,
              null,
              null,
              null
            ],
            internalRate: 6,
            peeringRatePlan: [
              {
                bound: 9,
                rate: 8
              }
            ]
          },
          extraIpAddresses: [
            null
          ],
          hostName: 'string',
          id: 'string',
          layer: 'string',
          live: false,
          logicalSystemParent: 'string',
          name: 'string',
          nodeIndex: 5,
          nodeType: 'string',
          prefixes: [
            {
              SR: {
                algo: 3,
                flags: 4,
                index: 1
              },
              SRv6: {
                algo: 1,
                flags: 2,
                metric: 3
              },
              role: 'string'
            },
            {
              SR: {
                algo: 2,
                flags: 10,
                index: 9
              },
              SRv6: {
                algo: 3,
                flags: 2,
                metric: 5
              },
              role: 'string'
            },
            {
              SR: {
                algo: 10,
                flags: 6,
                index: 3
              },
              SRv6: {
                algo: 7,
                flags: 10,
                metric: 9
              },
              role: 'string'
            },
            {
              SR: {
                algo: 6,
                flags: 1,
                index: 8
              },
              SRv6: {
                algo: 3,
                flags: 3,
                metric: 6
              },
              role: 'string'
            },
            {
              SR: {
                algo: 5,
                flags: 1,
                index: 7
              },
              SRv6: {
                algo: 5,
                flags: 4,
                metric: 9
              },
              role: 'string'
            },
            {
              SR: {
                algo: 6,
                flags: 10,
                index: 9
              },
              SRv6: {
                algo: 9,
                flags: 3,
                metric: 3
              },
              role: 'string'
            }
          ],
          protocols: {
            BGP: {
              AutonomousSystem: {
                asNumber: null
              },
              id: 'string'
            },
            ISIS: {
              TERouterId: 'string',
              TERouterIdIPv6: 'string',
              area: 'string',
              isoAddress: 'string',
              overloadBit: true,
              routerId: 'string',
              routerIdIPv6: 'string'
            },
            NETCONF: {
              clientAddress: null,
              clientCapabilities: [
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string',
                'string'
              ],
              operationalState: 'Invalid_credentials',
              operationalStatus: 'Up',
              sessionParameters: {
                keepalive: 10
              }
            },
            OSPF: {
              TERouterId: 'string',
              TERouterIdIPv6: 'string',
              overloadBit: true,
              referenceBw: 1,
              routerId: 'string',
              routerIdIPv6: 'string'
            },
            PCEP: {
              extensions: {
                'lsp-association-protection': true
              },
              operationalStatus: 'Empty',
              pccAddress: null,
              peerStatefullCapabilities: {
                SRCapability: false,
                SRv6Capability: true,
                lspInstantiation: false,
                lspUpdate: true,
                p2mp: true,
                p2mpInstantiation: false,
                p2mpUpdate: false
              },
              sessionParameters: {
                deadtimer: 7,
                keepalive: 8,
                maximumStackDepth: 5
              }
            },
            SR: {
              SRGBs: [
                {
                  range: 10,
                  start: 6
                },
                {
                  range: 2,
                  start: 8
                },
                {
                  range: 8,
                  start: 1
                }
              ],
              capabilities: {
                unrestrictedFirstHop: true
              },
              enabled: false,
              nodeCapabilities: 10
            },
            SRv6: {
              enabled: false,
              msdType: {
                baseMplsImposition: 4,
                erld: 10,
                srhMaxEndD: 9,
                srhMaxEndPop: 6,
                srhMaxHEncaps: 9,
                srhMaxLs: 2
              },
              nodeCapabilities: 3
            },
            management: {
              address: null,
              operatingSystem: 'string',
              operatingSystemVersion: 'string',
              vendor: 'string'
            }
          },
          pseudoNode: true,
          routerId: 'string',
          routerIdIPv6: 'string',
          site: 'string',
          slices: [
            {
              sliceId: 7
            },
            {
              sliceId: 5
            },
            {
              sliceId: 5
            },
            {
              sliceId: 4
            },
            {
              sliceId: 8
            },
            {
              sliceId: 4
            },
            {
              sliceId: 7
            },
            {
              sliceId: 1
            },
            {
              sliceId: 4
            }
          ],
          switchingCapabilities: [
            {
              encoding: 'lsp-encoding-oduk',
              switchingCapability: 'switching-l2sc'
            },
            {
              encoding: 'lsp-encoding-lambda',
              switchingCapability: 'switching-tdm'
            },
            {
              encoding: 'lsp-encoding-line',
              switchingCapability: 'switching-psc1'
            },
            {
              encoding: 'lsp-encoding-digital-wrapper',
              switchingCapability: 'switching-packet'
            },
            {
              encoding: 'lsp-encoding-pdh',
              switchingCapability: 'switching-evpl'
            },
            {
              encoding: 'lsp-encoding-pdh',
              switchingCapability: 'switching-evpl'
            }
          ],
          topoObjectType: 'node',
          topology: {
            coordinates: {
              bbox: [
                5,
                9,
                6,
                6,
                1
              ],
              crs: {
                properties: {},
                type: 'string'
              }
            }
          },
          topologyIndex: 1,
          userProperties: {}
        },
        prefix: null,
        priority: 7
      }
    ];
    describe('#putTopologyV2TopologyIdIpePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTopologyV2TopologyIdIpePolicy(555, topologyPutTopologyV2TopologyIdIpePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'putTopologyV2TopologyIdIpePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdIpePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyV2TopologyIdIpePolicy(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopologyV2TopologyIdIpePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdIpePolicyIpePolicyId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTopologyV2TopologyIdIpePolicyIpePolicyId(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-paragon_pathfinder-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getTopologyV2TopologyIdIpePolicyIpePolicyId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
