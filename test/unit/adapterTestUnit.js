/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-paragon_pathfinder',
      type: 'ParagonPathfinder',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const ParagonPathfinder = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Paragon_pathfinder Adapter Test', () => {
  describe('ParagonPathfinder Class Tests', () => {
    const a = new ParagonPathfinder(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('paragon_pathfinder'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('paragon_pathfinder'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('ParagonPathfinder', pronghornDotJson.export);
          assert.equal('Paragon_pathfinder', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-paragon_pathfinder', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('paragon_pathfinder'));
          assert.equal('ParagonPathfinder', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-paragon_pathfinder', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-paragon_pathfinder', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getTopologyV2 - errors', () => {
      it('should have a getTopologyV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyId - errors', () => {
      it('should have a getTopologyV2TopologyId function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyId(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyId - errors', () => {
      it('should have a deleteTopologyV2TopologyId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyV2TopologyId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteTopologyV2TopologyId(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdStream - errors', () => {
      it('should have a getTopologyV2TopologyIdStream function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdStream === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdStream(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdStream', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdStatus - errors', () => {
      it('should have a getTopologyV2TopologyIdStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdStatus(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdStatusAnalyticsCollection - errors', () => {
      it('should have a getTopologyV2TopologyIdStatusAnalyticsCollection function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdStatusAnalyticsCollection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdStatusAnalyticsCollection(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdStatusAnalyticsCollection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdStatusAnalyticsDatabase - errors', () => {
      it('should have a getTopologyV2TopologyIdStatusAnalyticsDatabase function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdStatusAnalyticsDatabase === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdStatusAnalyticsDatabase(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdStatusAnalyticsDatabase', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdStatusPce - errors', () => {
      it('should have a getTopologyV2TopologyIdStatusPce function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdStatusPce === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdStatusPce(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdStatusPce', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdStatusTopologyAcquisition - errors', () => {
      it('should have a getTopologyV2TopologyIdStatusTopologyAcquisition function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdStatusTopologyAcquisition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdStatusTopologyAcquisition(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdStatusTopologyAcquisition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdStatusPathComputationServer - errors', () => {
      it('should have a getTopologyV2TopologyIdStatusPathComputationServer function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdStatusPathComputationServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdStatusPathComputationServer(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdStatusPathComputationServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdStatusTransportTopologyAcquisition - errors', () => {
      it('should have a getTopologyV2TopologyIdStatusTransportTopologyAcquisition function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdStatusTransportTopologyAcquisition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdStatusTransportTopologyAcquisition(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdStatusTransportTopologyAcquisition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdNodes - errors', () => {
      it('should have a getTopologyV2TopologyIdNodes function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdNodes(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdNodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTopologyV2TopologyIdNodes - errors', () => {
      it('should have a postTopologyV2TopologyIdNodes function', (done) => {
        try {
          assert.equal(true, typeof a.postTopologyV2TopologyIdNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postTopologyV2TopologyIdNodes(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-postTopologyV2TopologyIdNodes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdNodesSearch - errors', () => {
      it('should have a getTopologyV2TopologyIdNodesSearch function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdNodesSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesSearch(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdNodesSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdNodesStream - errors', () => {
      it('should have a getTopologyV2TopologyIdNodesStream function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdNodesStream === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesStream(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdNodesStream', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTopologyV2TopologyIdNodesBulk - errors', () => {
      it('should have a postTopologyV2TopologyIdNodesBulk function', (done) => {
        try {
          assert.equal(true, typeof a.postTopologyV2TopologyIdNodesBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postTopologyV2TopologyIdNodesBulk(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-postTopologyV2TopologyIdNodesBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTopologyV2TopologyIdNodesBulk - errors', () => {
      it('should have a putTopologyV2TopologyIdNodesBulk function', (done) => {
        try {
          assert.equal(true, typeof a.putTopologyV2TopologyIdNodesBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.putTopologyV2TopologyIdNodesBulk(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdNodesBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTopologyV2TopologyIdNodesBulk - errors', () => {
      it('should have a patchTopologyV2TopologyIdNodesBulk function', (done) => {
        try {
          assert.equal(true, typeof a.patchTopologyV2TopologyIdNodesBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.patchTopologyV2TopologyIdNodesBulk(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdNodesBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdNodesBulk - errors', () => {
      it('should have a deleteTopologyV2TopologyIdNodesBulk function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyV2TopologyIdNodesBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteTopologyV2TopologyIdNodesBulk(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdNodesBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdNodesNodeIndex - errors', () => {
      it('should have a getTopologyV2TopologyIdNodesNodeIndex function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdNodesNodeIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesNodeIndex(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdNodesNodeIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesNodeIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdNodesNodeIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTopologyV2TopologyIdNodesNodeIndex - errors', () => {
      it('should have a putTopologyV2TopologyIdNodesNodeIndex function', (done) => {
        try {
          assert.equal(true, typeof a.putTopologyV2TopologyIdNodesNodeIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.putTopologyV2TopologyIdNodesNodeIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdNodesNodeIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeIndex', (done) => {
        try {
          a.putTopologyV2TopologyIdNodesNodeIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdNodesNodeIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTopologyV2TopologyIdNodesNodeIndex - errors', () => {
      it('should have a patchTopologyV2TopologyIdNodesNodeIndex function', (done) => {
        try {
          assert.equal(true, typeof a.patchTopologyV2TopologyIdNodesNodeIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.patchTopologyV2TopologyIdNodesNodeIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdNodesNodeIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeIndex', (done) => {
        try {
          a.patchTopologyV2TopologyIdNodesNodeIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdNodesNodeIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdNodesNodeIndex - errors', () => {
      it('should have a deleteTopologyV2TopologyIdNodesNodeIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyV2TopologyIdNodesNodeIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteTopologyV2TopologyIdNodesNodeIndex(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdNodesNodeIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeIndex', (done) => {
        try {
          a.deleteTopologyV2TopologyIdNodesNodeIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdNodesNodeIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdNodesNodeIndexHistory - errors', () => {
      it('should have a getTopologyV2TopologyIdNodesNodeIndexHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdNodesNodeIndexHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesNodeIndexHistory(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdNodesNodeIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesNodeIndexHistory('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdNodesNodeIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdNodesNodeIndexUser - errors', () => {
      it('should have a getTopologyV2TopologyIdNodesNodeIndexUser function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdNodesNodeIndexUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesNodeIndexUser(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdNodesNodeIndexUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesNodeIndexUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdNodesNodeIndexUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdNodesNodeIndexBgpLs - errors', () => {
      it('should have a getTopologyV2TopologyIdNodesNodeIndexBgpLs function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdNodesNodeIndexBgpLs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesNodeIndexBgpLs(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdNodesNodeIndexBgpLs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesNodeIndexBgpLs('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdNodesNodeIndexBgpLs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdNodesNodeIndexDevice - errors', () => {
      it('should have a getTopologyV2TopologyIdNodesNodeIndexDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdNodesNodeIndexDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesNodeIndexDevice(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdNodesNodeIndexDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdNodesNodeIndexDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdNodesNodeIndexDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdLinks - errors', () => {
      it('should have a getTopologyV2TopologyIdLinks function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdLinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdLinks(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdLinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTopologyV2TopologyIdLinks - errors', () => {
      it('should have a postTopologyV2TopologyIdLinks function', (done) => {
        try {
          assert.equal(true, typeof a.postTopologyV2TopologyIdLinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postTopologyV2TopologyIdLinks(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-postTopologyV2TopologyIdLinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdLinksStream - errors', () => {
      it('should have a getTopologyV2TopologyIdLinksStream function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdLinksStream === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksStream(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdLinksStream', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdLinksUtilization - errors', () => {
      it('should have a getTopologyV2TopologyIdLinksUtilization function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdLinksUtilization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksUtilization(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdLinksUtilization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdLinksSearch - errors', () => {
      it('should have a getTopologyV2TopologyIdLinksSearch function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdLinksSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksSearch(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdLinksSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdLinksLinkIndex - errors', () => {
      it('should have a getTopologyV2TopologyIdLinksLinkIndex function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdLinksLinkIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksLinkIndex(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdLinksLinkIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksLinkIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdLinksLinkIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTopologyV2TopologyIdLinksLinkIndex - errors', () => {
      it('should have a putTopologyV2TopologyIdLinksLinkIndex function', (done) => {
        try {
          assert.equal(true, typeof a.putTopologyV2TopologyIdLinksLinkIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.putTopologyV2TopologyIdLinksLinkIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdLinksLinkIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkIndex', (done) => {
        try {
          a.putTopologyV2TopologyIdLinksLinkIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'linkIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdLinksLinkIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTopologyV2TopologyIdLinksLinkIndex - errors', () => {
      it('should have a patchTopologyV2TopologyIdLinksLinkIndex function', (done) => {
        try {
          assert.equal(true, typeof a.patchTopologyV2TopologyIdLinksLinkIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.patchTopologyV2TopologyIdLinksLinkIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdLinksLinkIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkIndex', (done) => {
        try {
          a.patchTopologyV2TopologyIdLinksLinkIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'linkIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdLinksLinkIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdLinksLinkIndex - errors', () => {
      it('should have a deleteTopologyV2TopologyIdLinksLinkIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyV2TopologyIdLinksLinkIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteTopologyV2TopologyIdLinksLinkIndex(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdLinksLinkIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkIndex', (done) => {
        try {
          a.deleteTopologyV2TopologyIdLinksLinkIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdLinksLinkIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdLinksLinkIndexHistory - errors', () => {
      it('should have a getTopologyV2TopologyIdLinksLinkIndexHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdLinksLinkIndexHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksLinkIndexHistory(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdLinksLinkIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksLinkIndexHistory('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdLinksLinkIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdLinksLinkIndexDevice - errors', () => {
      it('should have a getTopologyV2TopologyIdLinksLinkIndexDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdLinksLinkIndexDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksLinkIndexDevice(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdLinksLinkIndexDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksLinkIndexDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdLinksLinkIndexDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTopologyV2TopologyIdLinksLinkIndexDevice - errors', () => {
      it('should have a putTopologyV2TopologyIdLinksLinkIndexDevice function', (done) => {
        try {
          assert.equal(true, typeof a.putTopologyV2TopologyIdLinksLinkIndexDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.putTopologyV2TopologyIdLinksLinkIndexDevice(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdLinksLinkIndexDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkIndex', (done) => {
        try {
          a.putTopologyV2TopologyIdLinksLinkIndexDevice('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'linkIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdLinksLinkIndexDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTopologyV2TopologyIdLinksLinkIndexDevice - errors', () => {
      it('should have a patchTopologyV2TopologyIdLinksLinkIndexDevice function', (done) => {
        try {
          assert.equal(true, typeof a.patchTopologyV2TopologyIdLinksLinkIndexDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.patchTopologyV2TopologyIdLinksLinkIndexDevice(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdLinksLinkIndexDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkIndex', (done) => {
        try {
          a.patchTopologyV2TopologyIdLinksLinkIndexDevice('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'linkIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdLinksLinkIndexDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdLinksLinkIndexUser - errors', () => {
      it('should have a getTopologyV2TopologyIdLinksLinkIndexUser function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdLinksLinkIndexUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksLinkIndexUser(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdLinksLinkIndexUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksLinkIndexUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdLinksLinkIndexUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTopologyV2TopologyIdLinksLinkIndexUser - errors', () => {
      it('should have a putTopologyV2TopologyIdLinksLinkIndexUser function', (done) => {
        try {
          assert.equal(true, typeof a.putTopologyV2TopologyIdLinksLinkIndexUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.putTopologyV2TopologyIdLinksLinkIndexUser(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdLinksLinkIndexUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkIndex', (done) => {
        try {
          a.putTopologyV2TopologyIdLinksLinkIndexUser('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'linkIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdLinksLinkIndexUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTopologyV2TopologyIdLinksLinkIndexUser - errors', () => {
      it('should have a patchTopologyV2TopologyIdLinksLinkIndexUser function', (done) => {
        try {
          assert.equal(true, typeof a.patchTopologyV2TopologyIdLinksLinkIndexUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.patchTopologyV2TopologyIdLinksLinkIndexUser(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdLinksLinkIndexUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkIndex', (done) => {
        try {
          a.patchTopologyV2TopologyIdLinksLinkIndexUser('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'linkIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdLinksLinkIndexUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdLinksLinkIndexBgpLs - errors', () => {
      it('should have a getTopologyV2TopologyIdLinksLinkIndexBgpLs function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdLinksLinkIndexBgpLs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksLinkIndexBgpLs(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdLinksLinkIndexBgpLs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdLinksLinkIndexBgpLs('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdLinksLinkIndexBgpLs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdTeLsps - errors', () => {
      it('should have a getTopologyV2TopologyIdTeLsps function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdTeLsps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdTeLsps(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdTeLsps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTopologyV2TopologyIdTeLsps - errors', () => {
      it('should have a postTopologyV2TopologyIdTeLsps function', (done) => {
        try {
          assert.equal(true, typeof a.postTopologyV2TopologyIdTeLsps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postTopologyV2TopologyIdTeLsps(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-postTopologyV2TopologyIdTeLsps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdTeLspsSearch - errors', () => {
      it('should have a getTopologyV2TopologyIdTeLspsSearch function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdTeLspsSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdTeLspsSearch(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdTeLspsSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdTeLspsStream - errors', () => {
      it('should have a getTopologyV2TopologyIdTeLspsStream function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdTeLspsStream === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdTeLspsStream(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdTeLspsStream', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTopologyV2TopologyIdTeLspsBulk - errors', () => {
      it('should have a postTopologyV2TopologyIdTeLspsBulk function', (done) => {
        try {
          assert.equal(true, typeof a.postTopologyV2TopologyIdTeLspsBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postTopologyV2TopologyIdTeLspsBulk(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-postTopologyV2TopologyIdTeLspsBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTopologyV2TopologyIdTeLspsBulk - errors', () => {
      it('should have a putTopologyV2TopologyIdTeLspsBulk function', (done) => {
        try {
          assert.equal(true, typeof a.putTopologyV2TopologyIdTeLspsBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.putTopologyV2TopologyIdTeLspsBulk(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdTeLspsBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTopologyV2TopologyIdTeLspsBulk - errors', () => {
      it('should have a patchTopologyV2TopologyIdTeLspsBulk function', (done) => {
        try {
          assert.equal(true, typeof a.patchTopologyV2TopologyIdTeLspsBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.patchTopologyV2TopologyIdTeLspsBulk(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdTeLspsBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdTeLspsBulk - errors', () => {
      it('should have a deleteTopologyV2TopologyIdTeLspsBulk function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyV2TopologyIdTeLspsBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteTopologyV2TopologyIdTeLspsBulk(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdTeLspsBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdTeLspsLspIndex - errors', () => {
      it('should have a getTopologyV2TopologyIdTeLspsLspIndex function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdTeLspsLspIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdTeLspsLspIndex(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdTeLspsLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lspIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdTeLspsLspIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'lspIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdTeLspsLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTopologyV2TopologyIdTeLspsLspIndex - errors', () => {
      it('should have a putTopologyV2TopologyIdTeLspsLspIndex function', (done) => {
        try {
          assert.equal(true, typeof a.putTopologyV2TopologyIdTeLspsLspIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.putTopologyV2TopologyIdTeLspsLspIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdTeLspsLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lspIndex', (done) => {
        try {
          a.putTopologyV2TopologyIdTeLspsLspIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'lspIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdTeLspsLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTopologyV2TopologyIdTeLspsLspIndex - errors', () => {
      it('should have a patchTopologyV2TopologyIdTeLspsLspIndex function', (done) => {
        try {
          assert.equal(true, typeof a.patchTopologyV2TopologyIdTeLspsLspIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.patchTopologyV2TopologyIdTeLspsLspIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdTeLspsLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lspIndex', (done) => {
        try {
          a.patchTopologyV2TopologyIdTeLspsLspIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'lspIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdTeLspsLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdTeLspsLspIndex - errors', () => {
      it('should have a deleteTopologyV2TopologyIdTeLspsLspIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyV2TopologyIdTeLspsLspIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteTopologyV2TopologyIdTeLspsLspIndex(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdTeLspsLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lspIndex', (done) => {
        try {
          a.deleteTopologyV2TopologyIdTeLspsLspIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'lspIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdTeLspsLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdTeLspsLspIndexHistory - errors', () => {
      it('should have a getTopologyV2TopologyIdTeLspsLspIndexHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdTeLspsLspIndexHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdTeLspsLspIndexHistory(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdTeLspsLspIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lspIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdTeLspsLspIndexHistory('fakeparam', null, (data, error) => {
            try {
              const displayE = 'lspIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdTeLspsLspIndexHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdDemands - errors', () => {
      it('should have a getTopologyV2TopologyIdDemands function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdDemands === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdDemands(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdDemands', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTopologyV2TopologyIdDemands - errors', () => {
      it('should have a postTopologyV2TopologyIdDemands function', (done) => {
        try {
          assert.equal(true, typeof a.postTopologyV2TopologyIdDemands === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postTopologyV2TopologyIdDemands(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-postTopologyV2TopologyIdDemands', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdDemandsStream - errors', () => {
      it('should have a getTopologyV2TopologyIdDemandsStream function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdDemandsStream === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdDemandsStream(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdDemandsStream', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdDemandsDemandIndex - errors', () => {
      it('should have a getTopologyV2TopologyIdDemandsDemandIndex function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdDemandsDemandIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdDemandsDemandIndex(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdDemandsDemandIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing demandIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdDemandsDemandIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'demandIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdDemandsDemandIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTopologyV2TopologyIdDemandsDemandIndex - errors', () => {
      it('should have a putTopologyV2TopologyIdDemandsDemandIndex function', (done) => {
        try {
          assert.equal(true, typeof a.putTopologyV2TopologyIdDemandsDemandIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.putTopologyV2TopologyIdDemandsDemandIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdDemandsDemandIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing demandIndex', (done) => {
        try {
          a.putTopologyV2TopologyIdDemandsDemandIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'demandIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdDemandsDemandIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTopologyV2TopologyIdDemandsDemandIndex - errors', () => {
      it('should have a patchTopologyV2TopologyIdDemandsDemandIndex function', (done) => {
        try {
          assert.equal(true, typeof a.patchTopologyV2TopologyIdDemandsDemandIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.patchTopologyV2TopologyIdDemandsDemandIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdDemandsDemandIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing demandIndex', (done) => {
        try {
          a.patchTopologyV2TopologyIdDemandsDemandIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'demandIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdDemandsDemandIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdDemandsDemandIndex - errors', () => {
      it('should have a deleteTopologyV2TopologyIdDemandsDemandIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyV2TopologyIdDemandsDemandIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteTopologyV2TopologyIdDemandsDemandIndex(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdDemandsDemandIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing demandIndex', (done) => {
        try {
          a.deleteTopologyV2TopologyIdDemandsDemandIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'demandIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdDemandsDemandIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTopologyV2TopologyIdDemandsBulk - errors', () => {
      it('should have a postTopologyV2TopologyIdDemandsBulk function', (done) => {
        try {
          assert.equal(true, typeof a.postTopologyV2TopologyIdDemandsBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postTopologyV2TopologyIdDemandsBulk(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-postTopologyV2TopologyIdDemandsBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTopologyV2TopologyIdDemandsBulk - errors', () => {
      it('should have a putTopologyV2TopologyIdDemandsBulk function', (done) => {
        try {
          assert.equal(true, typeof a.putTopologyV2TopologyIdDemandsBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.putTopologyV2TopologyIdDemandsBulk(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdDemandsBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTopologyV2TopologyIdDemandsBulk - errors', () => {
      it('should have a patchTopologyV2TopologyIdDemandsBulk function', (done) => {
        try {
          assert.equal(true, typeof a.patchTopologyV2TopologyIdDemandsBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.patchTopologyV2TopologyIdDemandsBulk(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdDemandsBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdDemandsBulk - errors', () => {
      it('should have a deleteTopologyV2TopologyIdDemandsBulk function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyV2TopologyIdDemandsBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteTopologyV2TopologyIdDemandsBulk(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdDemandsBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdP2mp - errors', () => {
      it('should have a getTopologyV2TopologyIdP2mp function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdP2mp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdP2mp(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdP2mp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTopologyV2TopologyIdP2mp - errors', () => {
      it('should have a postTopologyV2TopologyIdP2mp function', (done) => {
        try {
          assert.equal(true, typeof a.postTopologyV2TopologyIdP2mp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postTopologyV2TopologyIdP2mp(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-postTopologyV2TopologyIdP2mp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdP2mpStream - errors', () => {
      it('should have a getTopologyV2TopologyIdP2mpStream function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdP2mpStream === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdP2mpStream(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdP2mpStream', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdP2mpBulk - errors', () => {
      it('should have a deleteTopologyV2TopologyIdP2mpBulk function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyV2TopologyIdP2mpBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteTopologyV2TopologyIdP2mpBulk(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdP2mpBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdP2mpP2mpGroupIndex - errors', () => {
      it('should have a getTopologyV2TopologyIdP2mpP2mpGroupIndex function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdP2mpP2mpGroupIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdP2mpP2mpGroupIndex(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdP2mpP2mpGroupIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing p2mpGroupIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdP2mpP2mpGroupIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'p2mpGroupIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdP2mpP2mpGroupIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTopologyV2TopologyIdP2mpP2mpGroupIndex - errors', () => {
      it('should have a putTopologyV2TopologyIdP2mpP2mpGroupIndex function', (done) => {
        try {
          assert.equal(true, typeof a.putTopologyV2TopologyIdP2mpP2mpGroupIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.putTopologyV2TopologyIdP2mpP2mpGroupIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdP2mpP2mpGroupIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing p2mpGroupIndex', (done) => {
        try {
          a.putTopologyV2TopologyIdP2mpP2mpGroupIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'p2mpGroupIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdP2mpP2mpGroupIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdP2mpP2mpGroupIndex - errors', () => {
      it('should have a deleteTopologyV2TopologyIdP2mpP2mpGroupIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyV2TopologyIdP2mpP2mpGroupIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteTopologyV2TopologyIdP2mpP2mpGroupIndex(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdP2mpP2mpGroupIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing p2mpGroupIndex', (done) => {
        try {
          a.deleteTopologyV2TopologyIdP2mpP2mpGroupIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'p2mpGroupIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdP2mpP2mpGroupIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTopologyV2TopologyIdP2mpP2mpGroupIndex - errors', () => {
      it('should have a patchTopologyV2TopologyIdP2mpP2mpGroupIndex function', (done) => {
        try {
          assert.equal(true, typeof a.patchTopologyV2TopologyIdP2mpP2mpGroupIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.patchTopologyV2TopologyIdP2mpP2mpGroupIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdP2mpP2mpGroupIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing p2mpGroupIndex', (done) => {
        try {
          a.patchTopologyV2TopologyIdP2mpP2mpGroupIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'p2mpGroupIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdP2mpP2mpGroupIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTopologyV2TopologyIdP2mpP2mpGroupIndex - errors', () => {
      it('should have a postTopologyV2TopologyIdP2mpP2mpGroupIndex function', (done) => {
        try {
          assert.equal(true, typeof a.postTopologyV2TopologyIdP2mpP2mpGroupIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postTopologyV2TopologyIdP2mpP2mpGroupIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-postTopologyV2TopologyIdP2mpP2mpGroupIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing p2mpGroupIndex', (done) => {
        try {
          a.postTopologyV2TopologyIdP2mpP2mpGroupIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'p2mpGroupIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-postTopologyV2TopologyIdP2mpP2mpGroupIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex - errors', () => {
      it('should have a getTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing p2mpGroupIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'p2mpGroupIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lspIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'lspIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex - errors', () => {
      it('should have a deleteTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing p2mpGroupIndex', (done) => {
        try {
          a.deleteTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'p2mpGroupIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lspIndex', (done) => {
        try {
          a.deleteTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'lspIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdP2mpP2mpGroupIndexBulk - errors', () => {
      it('should have a deleteTopologyV2TopologyIdP2mpP2mpGroupIndexBulk function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyV2TopologyIdP2mpP2mpGroupIndexBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteTopologyV2TopologyIdP2mpP2mpGroupIndexBulk(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdP2mpP2mpGroupIndexBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing p2mpGroupIndex', (done) => {
        try {
          a.deleteTopologyV2TopologyIdP2mpP2mpGroupIndexBulk('fakeparam', null, (data, error) => {
            try {
              const displayE = 'p2mpGroupIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdP2mpP2mpGroupIndexBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdVrfs - errors', () => {
      it('should have a getTopologyV2TopologyIdVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdVrfs(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdVrfsNodeIndex - errors', () => {
      it('should have a getTopologyV2TopologyIdVrfsNodeIndex function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdVrfsNodeIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdVrfsNodeIndex(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdVrfsNodeIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdVrfsNodeIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdVrfsNodeIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdMaintenances - errors', () => {
      it('should have a getTopologyV2TopologyIdMaintenances function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdMaintenances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdMaintenances(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdMaintenances', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTopologyV2TopologyIdMaintenances - errors', () => {
      it('should have a postTopologyV2TopologyIdMaintenances function', (done) => {
        try {
          assert.equal(true, typeof a.postTopologyV2TopologyIdMaintenances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postTopologyV2TopologyIdMaintenances(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-postTopologyV2TopologyIdMaintenances', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdMaintenancesStream - errors', () => {
      it('should have a getTopologyV2TopologyIdMaintenancesStream function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdMaintenancesStream === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdMaintenancesStream(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdMaintenancesStream', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdMaintenancesMaintenanceIndex - errors', () => {
      it('should have a getTopologyV2TopologyIdMaintenancesMaintenanceIndex function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdMaintenancesMaintenanceIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdMaintenancesMaintenanceIndex(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdMaintenancesMaintenanceIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maintenanceIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdMaintenancesMaintenanceIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'maintenanceIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdMaintenancesMaintenanceIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTopologyV2TopologyIdMaintenancesMaintenanceIndex - errors', () => {
      it('should have a putTopologyV2TopologyIdMaintenancesMaintenanceIndex function', (done) => {
        try {
          assert.equal(true, typeof a.putTopologyV2TopologyIdMaintenancesMaintenanceIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.putTopologyV2TopologyIdMaintenancesMaintenanceIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdMaintenancesMaintenanceIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maintenanceIndex', (done) => {
        try {
          a.putTopologyV2TopologyIdMaintenancesMaintenanceIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'maintenanceIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdMaintenancesMaintenanceIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdMaintenancesMaintenanceIndex - errors', () => {
      it('should have a deleteTopologyV2TopologyIdMaintenancesMaintenanceIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyV2TopologyIdMaintenancesMaintenanceIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteTopologyV2TopologyIdMaintenancesMaintenanceIndex(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdMaintenancesMaintenanceIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maintenanceIndex', (done) => {
        try {
          a.deleteTopologyV2TopologyIdMaintenancesMaintenanceIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'maintenanceIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdMaintenancesMaintenanceIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdFacilities - errors', () => {
      it('should have a getTopologyV2TopologyIdFacilities function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdFacilities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdFacilities(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdFacilities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTopologyV2TopologyIdFacilities - errors', () => {
      it('should have a postTopologyV2TopologyIdFacilities function', (done) => {
        try {
          assert.equal(true, typeof a.postTopologyV2TopologyIdFacilities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postTopologyV2TopologyIdFacilities(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-postTopologyV2TopologyIdFacilities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdFacilitiesStream - errors', () => {
      it('should have a getTopologyV2TopologyIdFacilitiesStream function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdFacilitiesStream === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdFacilitiesStream(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdFacilitiesStream', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdFacilitiesFacilityIndex - errors', () => {
      it('should have a getTopologyV2TopologyIdFacilitiesFacilityIndex function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdFacilitiesFacilityIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdFacilitiesFacilityIndex(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdFacilitiesFacilityIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing facilityIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdFacilitiesFacilityIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'facilityIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdFacilitiesFacilityIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTopologyV2TopologyIdFacilitiesFacilityIndex - errors', () => {
      it('should have a putTopologyV2TopologyIdFacilitiesFacilityIndex function', (done) => {
        try {
          assert.equal(true, typeof a.putTopologyV2TopologyIdFacilitiesFacilityIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.putTopologyV2TopologyIdFacilitiesFacilityIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdFacilitiesFacilityIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing facilityIndex', (done) => {
        try {
          a.putTopologyV2TopologyIdFacilitiesFacilityIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'facilityIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdFacilitiesFacilityIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdFacilitiesFacilityIndex - errors', () => {
      it('should have a deleteTopologyV2TopologyIdFacilitiesFacilityIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyV2TopologyIdFacilitiesFacilityIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteTopologyV2TopologyIdFacilitiesFacilityIndex(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdFacilitiesFacilityIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing facilityIndex', (done) => {
        try {
          a.deleteTopologyV2TopologyIdFacilitiesFacilityIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'facilityIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdFacilitiesFacilityIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdTeContainers - errors', () => {
      it('should have a getTopologyV2TopologyIdTeContainers function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdTeContainers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdTeContainers(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdTeContainers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTopologyV2TopologyIdTeContainers - errors', () => {
      it('should have a postTopologyV2TopologyIdTeContainers function', (done) => {
        try {
          assert.equal(true, typeof a.postTopologyV2TopologyIdTeContainers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postTopologyV2TopologyIdTeContainers(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-postTopologyV2TopologyIdTeContainers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdTeContainersStream - errors', () => {
      it('should have a getTopologyV2TopologyIdTeContainersStream function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdTeContainersStream === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdTeContainersStream(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdTeContainersStream', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdTeContainersContainerIndex - errors', () => {
      it('should have a getTopologyV2TopologyIdTeContainersContainerIndex function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdTeContainersContainerIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdTeContainersContainerIndex(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdTeContainersContainerIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdTeContainersContainerIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'containerIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdTeContainersContainerIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTopologyV2TopologyIdTeContainersContainerIndex - errors', () => {
      it('should have a putTopologyV2TopologyIdTeContainersContainerIndex function', (done) => {
        try {
          assert.equal(true, typeof a.putTopologyV2TopologyIdTeContainersContainerIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.putTopologyV2TopologyIdTeContainersContainerIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdTeContainersContainerIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerIndex', (done) => {
        try {
          a.putTopologyV2TopologyIdTeContainersContainerIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdTeContainersContainerIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTopologyV2TopologyIdTeContainersContainerIndex - errors', () => {
      it('should have a patchTopologyV2TopologyIdTeContainersContainerIndex function', (done) => {
        try {
          assert.equal(true, typeof a.patchTopologyV2TopologyIdTeContainersContainerIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.patchTopologyV2TopologyIdTeContainersContainerIndex(null, null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdTeContainersContainerIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerIndex', (done) => {
        try {
          a.patchTopologyV2TopologyIdTeContainersContainerIndex('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdTeContainersContainerIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdTeContainersContainerIndex - errors', () => {
      it('should have a deleteTopologyV2TopologyIdTeContainersContainerIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyV2TopologyIdTeContainersContainerIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteTopologyV2TopologyIdTeContainersContainerIndex(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdTeContainersContainerIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerIndex', (done) => {
        try {
          a.deleteTopologyV2TopologyIdTeContainersContainerIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'containerIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdTeContainersContainerIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTopologyV2TopologyIdTeContainersBulk - errors', () => {
      it('should have a postTopologyV2TopologyIdTeContainersBulk function', (done) => {
        try {
          assert.equal(true, typeof a.postTopologyV2TopologyIdTeContainersBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postTopologyV2TopologyIdTeContainersBulk(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-postTopologyV2TopologyIdTeContainersBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTopologyV2TopologyIdTeContainersBulk - errors', () => {
      it('should have a putTopologyV2TopologyIdTeContainersBulk function', (done) => {
        try {
          assert.equal(true, typeof a.putTopologyV2TopologyIdTeContainersBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.putTopologyV2TopologyIdTeContainersBulk(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdTeContainersBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTopologyV2TopologyIdTeContainersBulk - errors', () => {
      it('should have a patchTopologyV2TopologyIdTeContainersBulk function', (done) => {
        try {
          assert.equal(true, typeof a.patchTopologyV2TopologyIdTeContainersBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.patchTopologyV2TopologyIdTeContainersBulk(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-patchTopologyV2TopologyIdTeContainersBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdTeContainersBulk - errors', () => {
      it('should have a deleteTopologyV2TopologyIdTeContainersBulk function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyV2TopologyIdTeContainersBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteTopologyV2TopologyIdTeContainersBulk(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdTeContainersBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdRoutes - errors', () => {
      it('should have a getTopologyV2TopologyIdRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdRoutes(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdRoutes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdRoutesNodeIndex - errors', () => {
      it('should have a getTopologyV2TopologyIdRoutesNodeIndex function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdRoutesNodeIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdRoutesNodeIndex(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdRoutesNodeIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeIndex', (done) => {
        try {
          a.getTopologyV2TopologyIdRoutesNodeIndex('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdRoutesNodeIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTopologyV2TopologyIdPathComputation - errors', () => {
      it('should have a postTopologyV2TopologyIdPathComputation function', (done) => {
        try {
          assert.equal(true, typeof a.postTopologyV2TopologyIdPathComputation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postTopologyV2TopologyIdPathComputation(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-postTopologyV2TopologyIdPathComputation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTopologyV2RpcOptimize - errors', () => {
      it('should have a postTopologyV2RpcOptimize function', (done) => {
        try {
          assert.equal(true, typeof a.postTopologyV2RpcOptimize === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2RpcOptimize - errors', () => {
      it('should have a getTopologyV2RpcOptimize function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2RpcOptimize === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTopologyV2RpcSimulation - errors', () => {
      it('should have a postTopologyV2RpcSimulation function', (done) => {
        try {
          assert.equal(true, typeof a.postTopologyV2RpcSimulation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2RpcSimulation - errors', () => {
      it('should have a getTopologyV2RpcSimulation function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2RpcSimulation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2RpcSimulationUuid - errors', () => {
      it('should have a getTopologyV2RpcSimulationUuid function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2RpcSimulationUuid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getTopologyV2RpcSimulationUuid(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2RpcSimulationUuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2RpcSimulationUuidReportName - errors', () => {
      it('should have a getTopologyV2RpcSimulationUuidReportName function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2RpcSimulationUuidReportName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getTopologyV2RpcSimulationUuidReportName(null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2RpcSimulationUuidReportName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportName', (done) => {
        try {
          a.getTopologyV2RpcSimulationUuidReportName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'reportName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2RpcSimulationUuidReportName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTopologyV2RpcDiverseTreeDesign - errors', () => {
      it('should have a postTopologyV2RpcDiverseTreeDesign function', (done) => {
        try {
          assert.equal(true, typeof a.postTopologyV2RpcDiverseTreeDesign === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2RpcDiverseTreeDesign - errors', () => {
      it('should have a getTopologyV2RpcDiverseTreeDesign function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2RpcDiverseTreeDesign === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2RpcDiverseTreeDesignUuid - errors', () => {
      it('should have a getTopologyV2RpcDiverseTreeDesignUuid function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2RpcDiverseTreeDesignUuid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getTopologyV2RpcDiverseTreeDesignUuid(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2RpcDiverseTreeDesignUuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchedulerV2Tasklist - errors', () => {
      it('should have a getSchedulerV2Tasklist function', (done) => {
        try {
          assert.equal(true, typeof a.getSchedulerV2Tasklist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchedulerV2Tasks - errors', () => {
      it('should have a getSchedulerV2Tasks function', (done) => {
        try {
          assert.equal(true, typeof a.getSchedulerV2Tasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSchedulerV2Addtask - errors', () => {
      it('should have a postSchedulerV2Addtask function', (done) => {
        try {
          assert.equal(true, typeof a.postSchedulerV2Addtask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSchedulerV2Updatetask - errors', () => {
      it('should have a postSchedulerV2Updatetask function', (done) => {
        try {
          assert.equal(true, typeof a.postSchedulerV2Updatetask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSchedulerV2Removeaddtask - errors', () => {
      it('should have a postSchedulerV2Removeaddtask function', (done) => {
        try {
          assert.equal(true, typeof a.postSchedulerV2Removeaddtask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSchedulerV2Deletetask - errors', () => {
      it('should have a postSchedulerV2Deletetask function', (done) => {
        try {
          assert.equal(true, typeof a.postSchedulerV2Deletetask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchedulerV2TaskshistoryTaskId - errors', () => {
      it('should have a getSchedulerV2TaskshistoryTaskId function', (done) => {
        try {
          assert.equal(true, typeof a.getSchedulerV2TaskshistoryTaskId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getSchedulerV2TaskshistoryTaskId(null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getSchedulerV2TaskshistoryTaskId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchedulerV2TaskstatusTaskId - errors', () => {
      it('should have a getSchedulerV2TaskstatusTaskId function', (done) => {
        try {
          assert.equal(true, typeof a.getSchedulerV2TaskstatusTaskId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getSchedulerV2TaskstatusTaskId(null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getSchedulerV2TaskstatusTaskId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComponentConfig - errors', () => {
      it('should have a getComponentConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getComponentConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postConfig - errors', () => {
      it('should have a postConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchConfig - errors', () => {
      it('should have a patchConfig function', (done) => {
        try {
          assert.equal(true, typeof a.patchConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConfig - errors', () => {
      it('should have a deleteConfig function', (done) => {
        try {
          assert.equal(true, typeof a.deleteConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportControllersV2 - errors', () => {
      it('should have a getTransportControllersV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportControllersV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransportControllersV2 - errors', () => {
      it('should have a postTransportControllersV2 function', (done) => {
        try {
          assert.equal(true, typeof a.postTransportControllersV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportControllersV2TransportControllerIndex - errors', () => {
      it('should have a getTransportControllersV2TransportControllerIndex function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportControllersV2TransportControllerIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportControllerIndex', (done) => {
        try {
          a.getTransportControllersV2TransportControllerIndex(null, (data, error) => {
            try {
              const displayE = 'transportControllerIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTransportControllersV2TransportControllerIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTransportControllersV2TransportControllerIndex - errors', () => {
      it('should have a putTransportControllersV2TransportControllerIndex function', (done) => {
        try {
          assert.equal(true, typeof a.putTransportControllersV2TransportControllerIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportControllerIndex', (done) => {
        try {
          a.putTransportControllersV2TransportControllerIndex(null, null, (data, error) => {
            try {
              const displayE = 'transportControllerIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTransportControllersV2TransportControllerIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransportControllersV2TransportControllerIndex - errors', () => {
      it('should have a deleteTransportControllersV2TransportControllerIndex function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTransportControllersV2TransportControllerIndex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportControllerIndex', (done) => {
        try {
          a.deleteTransportControllersV2TransportControllerIndex(null, (data, error) => {
            try {
              const displayE = 'transportControllerIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTransportControllersV2TransportControllerIndex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportControllerGroupsV2 - errors', () => {
      it('should have a getTransportControllerGroupsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportControllerGroupsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransportControllerGroupsV2 - errors', () => {
      it('should have a postTransportControllerGroupsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.postTransportControllerGroupsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransportControllerGroupsV2 - errors', () => {
      it('should have a deleteTransportControllerGroupsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTransportControllerGroupsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportControllerGroupsV2TransportControllerGroupName - errors', () => {
      it('should have a getTransportControllerGroupsV2TransportControllerGroupName function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportControllerGroupsV2TransportControllerGroupName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportControllerGroupName', (done) => {
        try {
          a.getTransportControllerGroupsV2TransportControllerGroupName(null, (data, error) => {
            try {
              const displayE = 'transportControllerGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTransportControllerGroupsV2TransportControllerGroupName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTransportControllerGroupsV2TransportControllerGroupName - errors', () => {
      it('should have a putTransportControllerGroupsV2TransportControllerGroupName function', (done) => {
        try {
          assert.equal(true, typeof a.putTransportControllerGroupsV2TransportControllerGroupName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportControllerGroupName', (done) => {
        try {
          a.putTransportControllerGroupsV2TransportControllerGroupName(null, null, (data, error) => {
            try {
              const displayE = 'transportControllerGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTransportControllerGroupsV2TransportControllerGroupName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTransportControllerGroupsV2TransportControllerGroupName - errors', () => {
      it('should have a postTransportControllerGroupsV2TransportControllerGroupName function', (done) => {
        try {
          assert.equal(true, typeof a.postTransportControllerGroupsV2TransportControllerGroupName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportControllerGroupName', (done) => {
        try {
          a.postTransportControllerGroupsV2TransportControllerGroupName(null, null, (data, error) => {
            try {
              const displayE = 'transportControllerGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-postTransportControllerGroupsV2TransportControllerGroupName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransportControllerGroupsV2TransportControllerGroupName - errors', () => {
      it('should have a deleteTransportControllerGroupsV2TransportControllerGroupName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTransportControllerGroupsV2TransportControllerGroupName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transportControllerGroupName', (done) => {
        try {
          a.deleteTransportControllerGroupsV2TransportControllerGroupName(null, (data, error) => {
            try {
              const displayE = 'transportControllerGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTransportControllerGroupsV2TransportControllerGroupName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetconfV2Profiles - errors', () => {
      it('should have a getNetconfV2Profiles function', (done) => {
        try {
          assert.equal(true, typeof a.getNetconfV2Profiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetconfV2Profiles - errors', () => {
      it('should have a postNetconfV2Profiles function', (done) => {
        try {
          assert.equal(true, typeof a.postNetconfV2Profiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putNetconfV2Profiles - errors', () => {
      it('should have a putNetconfV2Profiles function', (done) => {
        try {
          assert.equal(true, typeof a.putNetconfV2Profiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetconfV2Profiles - errors', () => {
      it('should have a deleteNetconfV2Profiles function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetconfV2Profiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetconfV2CollectionLiveNetwork - errors', () => {
      it('should have a postNetconfV2CollectionLiveNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.postNetconfV2CollectionLiveNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetconfV2CollectionCollectionJobId - errors', () => {
      it('should have a getNetconfV2CollectionCollectionJobId function', (done) => {
        try {
          assert.equal(true, typeof a.getNetconfV2CollectionCollectionJobId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing collectionJobId', (done) => {
        try {
          a.getNetconfV2CollectionCollectionJobId(null, (data, error) => {
            try {
              const displayE = 'collectionJobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getNetconfV2CollectionCollectionJobId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdIpePolicy - errors', () => {
      it('should have a getTopologyV2TopologyIdIpePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdIpePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdIpePolicy(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdIpePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTopologyV2TopologyIdIpePolicy - errors', () => {
      it('should have a postTopologyV2TopologyIdIpePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.postTopologyV2TopologyIdIpePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.postTopologyV2TopologyIdIpePolicy(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-postTopologyV2TopologyIdIpePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTopologyV2TopologyIdIpePolicy - errors', () => {
      it('should have a putTopologyV2TopologyIdIpePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.putTopologyV2TopologyIdIpePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.putTopologyV2TopologyIdIpePolicy(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-putTopologyV2TopologyIdIpePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyV2TopologyIdIpePolicy - errors', () => {
      it('should have a deleteTopologyV2TopologyIdIpePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyV2TopologyIdIpePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.deleteTopologyV2TopologyIdIpePolicy(null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-deleteTopologyV2TopologyIdIpePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyV2TopologyIdIpePolicyIpePolicyId - errors', () => {
      it('should have a getTopologyV2TopologyIdIpePolicyIpePolicyId function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyV2TopologyIdIpePolicyIpePolicyId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topologyId', (done) => {
        try {
          a.getTopologyV2TopologyIdIpePolicyIpePolicyId(null, null, (data, error) => {
            try {
              const displayE = 'topologyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdIpePolicyIpePolicyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipePolicyId', (done) => {
        try {
          a.getTopologyV2TopologyIdIpePolicyIpePolicyId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ipePolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_pathfinder-adapter-getTopologyV2TopologyIdIpePolicyIpePolicyId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
