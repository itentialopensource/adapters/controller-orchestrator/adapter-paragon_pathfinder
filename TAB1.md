# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Paragon_pathfinder System. The API that was used to build the adapter for Paragon_pathfinder is usually available in the report directory of this adapter. The adapter utilizes the Paragon_pathfinder API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Paragon Pathfinder adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Juniper Paragon Pathfinder. With this adapter you have the ability to perform operations such as:

- Topology
- Config
- NETCONF

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
