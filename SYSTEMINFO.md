# Paragon Pathfinder

Vendor: Juniper Networks
Homepage: https://www.juniper.net/us/en.html

Product: Paragon Pathfinder
Product Page: https://www.juniper.net/us/en/products/network-automation/paragon-pathfinder.html

## Introduction
We classify Paragon Pathfinder into the Network Services domain as Paragon Pathfinder provides functionalities centered around optimizing and managing network traffic. We also classify Paragon Pathfinder into the Data Center domain due to its role in managing and optimizing network infrastructure within data center.

"Paragon Pathfinder enables your operations team to more efficiently manage strict transport service SLAs through automated planning, provisioning, proactive monitoring, and optimization of large traffic loads dynamically" 

## Why Integrate
The Paragon Pathfinder adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Juniper Paragon Pathfinder. With this adapter you have the ability to perform operations such as:

- Topology
- Config
- NETCONF

## Additional Product Documentation
The [API documents for Paragon Pathfinder](https://www.juniper.net/documentation/us/en/software/paragon-automation23.2/paragon-automation-api-ref/paragon-pathfinder/api-ref-pathfinder-v2.html)