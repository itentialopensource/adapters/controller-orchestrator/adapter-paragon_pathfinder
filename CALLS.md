## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Juniper Paragon Pathfinder. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Juniper Paragon Pathfinder.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Paragon_pathfinder. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2(callback)</td>
    <td style="padding:15px">Get List of Topologies</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyId(topologyId, callback)</td>
    <td style="padding:15px">Get Topological Elements</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyV2TopologyId(topologyId, callback)</td>
    <td style="padding:15px">Delete the Topology</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdStream(topologyId, callback)</td>
    <td style="padding:15px">Start a SSE Stream</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/stream?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdStatus(topologyId, callback)</td>
    <td style="padding:15px">Get Paragon PathFinder Component Status</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdStatusAnalyticsCollection(topologyId, callback)</td>
    <td style="padding:15px">Get Analytics Data Collection Component Status</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/status/analyticsCollection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdStatusAnalyticsDatabase(topologyId, callback)</td>
    <td style="padding:15px">Get Analytics Data Collection Database Status</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/status/analyticsDatabase?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdStatusPce(topologyId, callback)</td>
    <td style="padding:15px">Get PCEP Protocol Adapter Status</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/status/pce?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdStatusTopologyAcquisition(topologyId, callback)</td>
    <td style="padding:15px">Get BGP-LS Protocol Adapter Status</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/status/topologyAcquisition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdStatusPathComputationServer(topologyId, callback)</td>
    <td style="padding:15px">Get Path Computation Server Status</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/status/pathComputationServer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdStatusTransportTopologyAcquisition(topologyId, callback)</td>
    <td style="padding:15px">Get Transport Topology Acquisition Daemon Status</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/status/transportTopologyAcquisition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdNodes(topologyId, callback)</td>
    <td style="padding:15px">Get Nodes</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTopologyV2TopologyIdNodes(topologyId, body, callback)</td>
    <td style="padding:15px">Create a Node</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdNodesSearch(topologyId, queryData, callback)</td>
    <td style="padding:15px">Search Nodes</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/nodes/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdNodesStream(topologyId, callback)</td>
    <td style="padding:15px">Start a SSE Stream</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/nodes/stream?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTopologyV2TopologyIdNodesBulk(topologyId, body, callback)</td>
    <td style="padding:15px">Create a List of Nodes</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/nodes/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTopologyV2TopologyIdNodesBulk(topologyId, body, callback)</td>
    <td style="padding:15px">Update a List of Nodes</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/nodes/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTopologyV2TopologyIdNodesBulk(topologyId, body, callback)</td>
    <td style="padding:15px">Update a List of Nodes using PATCH</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/nodes/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyV2TopologyIdNodesBulk(topologyId, callback)</td>
    <td style="padding:15px">Delete a List of Nodes</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/nodes/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdNodesNodeIndex(topologyId, nodeIndex, callback)</td>
    <td style="padding:15px">Get a Single Node</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTopologyV2TopologyIdNodesNodeIndex(topologyId, nodeIndex, body, callback)</td>
    <td style="padding:15px">Update Node Properties</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTopologyV2TopologyIdNodesNodeIndex(topologyId, nodeIndex, body, callback)</td>
    <td style="padding:15px">Patch Node</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyV2TopologyIdNodesNodeIndex(topologyId, nodeIndex, callback)</td>
    <td style="padding:15px">Delete a Node</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdNodesNodeIndexHistory(topologyId, nodeIndex, callback)</td>
    <td style="padding:15px">Get the Node Event History</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/nodes/{pathv2}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdNodesNodeIndexUser(topologyId, nodeIndex, callback)</td>
    <td style="padding:15px">Get the node user-defined information</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/nodes/{pathv2}/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdNodesNodeIndexBgpLs(topologyId, nodeIndex, callback)</td>
    <td style="padding:15px">Get the bgp-ls node information</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/nodes/{pathv2}/bgp-ls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdNodesNodeIndexDevice(topologyId, nodeIndex, callback)</td>
    <td style="padding:15px">Get the link device information</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/nodes/{pathv2}/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdLinks(topologyId, callback)</td>
    <td style="padding:15px">Get Links</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTopologyV2TopologyIdLinks(topologyId, body, callback)</td>
    <td style="padding:15px">Create a Link</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdLinksStream(topologyId, callback)</td>
    <td style="padding:15px">Start a SSE Stream</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/links/stream?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdLinksUtilization(topologyId, callback)</td>
    <td style="padding:15px">Get Links utilization only</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/links/utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdLinksSearch(topologyId, queryData, callback)</td>
    <td style="padding:15px">Search Links</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/links/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdLinksLinkIndex(topologyId, linkIndex, callback)</td>
    <td style="padding:15px">Get a Single Link</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/links/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTopologyV2TopologyIdLinksLinkIndex(topologyId, linkIndex, body, callback)</td>
    <td style="padding:15px">Update Link</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/links/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTopologyV2TopologyIdLinksLinkIndex(topologyId, linkIndex, body, callback)</td>
    <td style="padding:15px">Patch Link</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/links/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyV2TopologyIdLinksLinkIndex(topologyId, linkIndex, callback)</td>
    <td style="padding:15px">Delete a Link</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/links/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdLinksLinkIndexHistory(topologyId, linkIndex, callback)</td>
    <td style="padding:15px">Get the Link Event History</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/links/{pathv2}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdLinksLinkIndexDevice(topologyId, linkIndex, callback)</td>
    <td style="padding:15px">Get the link device information</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/links/{pathv2}/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTopologyV2TopologyIdLinksLinkIndexDevice(topologyId, linkIndex, body, callback)</td>
    <td style="padding:15px">Update Link</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/links/{pathv2}/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTopologyV2TopologyIdLinksLinkIndexDevice(topologyId, linkIndex, body, callback)</td>
    <td style="padding:15px">Patch Link</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/links/{pathv2}/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdLinksLinkIndexUser(topologyId, linkIndex, callback)</td>
    <td style="padding:15px">Get the link user-defined information</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/links/{pathv2}/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTopologyV2TopologyIdLinksLinkIndexUser(topologyId, linkIndex, body, callback)</td>
    <td style="padding:15px">Update Link</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/links/{pathv2}/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTopologyV2TopologyIdLinksLinkIndexUser(topologyId, linkIndex, body, callback)</td>
    <td style="padding:15px">Patch Link</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/links/{pathv2}/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdLinksLinkIndexBgpLs(topologyId, linkIndex, callback)</td>
    <td style="padding:15px">Get the link Live information</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/links/{pathv2}/bgp-ls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdTeLsps(topologyId, callback)</td>
    <td style="padding:15px">Get TE-LSPs</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-lsps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTopologyV2TopologyIdTeLsps(topologyId, body, callback)</td>
    <td style="padding:15px">Create a TE-LSP</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-lsps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdTeLspsSearch(topologyId, queryData, callback)</td>
    <td style="padding:15px">Search LSPs</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-lsps/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdTeLspsStream(topologyId, callback)</td>
    <td style="padding:15px">Start a SSE Stream</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-lsps/stream?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTopologyV2TopologyIdTeLspsBulk(topologyId, body, callback)</td>
    <td style="padding:15px">Create a List of TE-LSPs</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-lsps/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTopologyV2TopologyIdTeLspsBulk(topologyId, body, callback)</td>
    <td style="padding:15px">Update a List of TE-LSPs</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-lsps/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTopologyV2TopologyIdTeLspsBulk(topologyId, body, callback)</td>
    <td style="padding:15px">Update a List of TE-LSPs using PATCH</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-lsps/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyV2TopologyIdTeLspsBulk(topologyId, callback)</td>
    <td style="padding:15px">Delete a List of TE-LSPs</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-lsps/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdTeLspsLspIndex(topologyId, lspIndex, callback)</td>
    <td style="padding:15px">Get a Single TE-LSP</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-lsps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTopologyV2TopologyIdTeLspsLspIndex(topologyId, lspIndex, body, callback)</td>
    <td style="padding:15px">Update a TE-LSP</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-lsps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTopologyV2TopologyIdTeLspsLspIndex(topologyId, lspIndex, body, callback)</td>
    <td style="padding:15px">Patch a TE-LSP</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-lsps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyV2TopologyIdTeLspsLspIndex(topologyId, lspIndex, callback)</td>
    <td style="padding:15px">Delete a TE-LSP</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-lsps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdTeLspsLspIndexHistory(topologyId, lspIndex, callback)</td>
    <td style="padding:15px">Get the LSP Event History</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-lsps/{pathv2}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdDemands(topologyId, callback)</td>
    <td style="padding:15px">Gets all Demands</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/demands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTopologyV2TopologyIdDemands(topologyId, body, callback)</td>
    <td style="padding:15px">Create a Demand</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/demands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdDemandsStream(topologyId, callback)</td>
    <td style="padding:15px">Start a SSE Stream</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/demands/stream?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdDemandsDemandIndex(topologyId, demandIndex, callback)</td>
    <td style="padding:15px">Get a Specific Demand</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/demands/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTopologyV2TopologyIdDemandsDemandIndex(topologyId, demandIndex, body, callback)</td>
    <td style="padding:15px">Update a Specific Demand</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/demands/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTopologyV2TopologyIdDemandsDemandIndex(topologyId, demandIndex, body, callback)</td>
    <td style="padding:15px">Update a Specific Demand using PATCH</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/demands/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyV2TopologyIdDemandsDemandIndex(topologyId, demandIndex, callback)</td>
    <td style="padding:15px">Delete a Specific Demand</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/demands/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTopologyV2TopologyIdDemandsBulk(topologyId, body, callback)</td>
    <td style="padding:15px">Create a set of Demands</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/demands/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTopologyV2TopologyIdDemandsBulk(topologyId, body, callback)</td>
    <td style="padding:15px">Update a set of Demands</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/demands/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTopologyV2TopologyIdDemandsBulk(topologyId, body, callback)</td>
    <td style="padding:15px">Patch a set of Demands</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/demands/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyV2TopologyIdDemandsBulk(topologyId, callback)</td>
    <td style="padding:15px">Delete a set of Demands</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/demands/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdP2mp(topologyId, callback)</td>
    <td style="padding:15px">Get P2MP groups</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/p2mp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTopologyV2TopologyIdP2mp(topologyId, body, callback)</td>
    <td style="padding:15px">Create a P2MP group</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/p2mp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdP2mpStream(topologyId, callback)</td>
    <td style="padding:15px">Start a SSE Stream</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/p2mp/stream?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyV2TopologyIdP2mpBulk(topologyId, callback)</td>
    <td style="padding:15px">Delete list of P2MP Groups</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/p2mp/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdP2mpP2mpGroupIndex(topologyId, p2mpGroupIndex, callback)</td>
    <td style="padding:15px">Get a Specific P2MP Group</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/p2mp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTopologyV2TopologyIdP2mpP2mpGroupIndex(topologyId, p2mpGroupIndex, body, callback)</td>
    <td style="padding:15px">Update a Specific P2MP Group</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/p2mp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyV2TopologyIdP2mpP2mpGroupIndex(topologyId, p2mpGroupIndex, callback)</td>
    <td style="padding:15px">Delete a P2MP Group</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/p2mp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTopologyV2TopologyIdP2mpP2mpGroupIndex(topologyId, p2mpGroupIndex, body, callback)</td>
    <td style="padding:15px">Update a Specific P2MP Group using a PATCH document</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/p2mp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTopologyV2TopologyIdP2mpP2mpGroupIndex(topologyId, p2mpGroupIndex, body, callback)</td>
    <td style="padding:15px">Create a P2MP Leaf</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/p2mp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex(topologyId, p2mpGroupIndex, lspIndex, callback)</td>
    <td style="padding:15px">Get a Specific P2MP Leaf</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/p2mp/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyV2TopologyIdP2mpP2mpGroupIndexLspIndex(topologyId, p2mpGroupIndex, lspIndex, callback)</td>
    <td style="padding:15px">Delete a P2MP Leaf</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/p2mp/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyV2TopologyIdP2mpP2mpGroupIndexBulk(topologyId, p2mpGroupIndex, callback)</td>
    <td style="padding:15px">Delete list of P2MP leaves</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/p2mp/{pathv2}/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdVrfs(topologyId, callback)</td>
    <td style="padding:15px">Get the nodes with VRF</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/vrfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdVrfsNodeIndex(topologyId, nodeIndex, callback)</td>
    <td style="padding:15px">List the VRF on a given node</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/vrfs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdMaintenances(topologyId, callback)</td>
    <td style="padding:15px">Get Maintenance</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/maintenances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTopologyV2TopologyIdMaintenances(topologyId, body, callback)</td>
    <td style="padding:15px">Create a new maintenance</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/maintenances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdMaintenancesStream(topologyId, callback)</td>
    <td style="padding:15px">Start a SSE Stream</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/maintenances/stream?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdMaintenancesMaintenanceIndex(topologyId, maintenanceIndex, callback)</td>
    <td style="padding:15px">Get a Specific Maintenance</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/maintenances/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTopologyV2TopologyIdMaintenancesMaintenanceIndex(topologyId, maintenanceIndex, body, callback)</td>
    <td style="padding:15px">Update a Specific Maintenance</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/maintenances/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyV2TopologyIdMaintenancesMaintenanceIndex(topologyId, maintenanceIndex, callback)</td>
    <td style="padding:15px">Delete a Specific Maintenance</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/maintenances/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdFacilities(topologyId, callback)</td>
    <td style="padding:15px">Get all facilities</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/facilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTopologyV2TopologyIdFacilities(topologyId, body, callback)</td>
    <td style="padding:15px">Create a facilities</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/facilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdFacilitiesStream(topologyId, callback)</td>
    <td style="padding:15px">Start a SSE Stream</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/facilities/stream?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdFacilitiesFacilityIndex(topologyId, facilityIndex, callback)</td>
    <td style="padding:15px">Get one facility</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/facilities/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTopologyV2TopologyIdFacilitiesFacilityIndex(topologyId, facilityIndex, body, callback)</td>
    <td style="padding:15px">Update a facility</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/facilities/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyV2TopologyIdFacilitiesFacilityIndex(topologyId, facilityIndex, callback)</td>
    <td style="padding:15px">Delete the facility</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/facilities/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdTeContainers(topologyId, callback)</td>
    <td style="padding:15px">Get all TE Containers</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-containers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTopologyV2TopologyIdTeContainers(topologyId, body, callback)</td>
    <td style="padding:15px">Create a new te-container</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-containers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdTeContainersStream(topologyId, callback)</td>
    <td style="padding:15px">Start a SSE Stream</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-containers/stream?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdTeContainersContainerIndex(topologyId, containerIndex, callback)</td>
    <td style="padding:15px">Get one te-container</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-containers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTopologyV2TopologyIdTeContainersContainerIndex(topologyId, containerIndex, body, callback)</td>
    <td style="padding:15px">Update the te-container</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-containers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTopologyV2TopologyIdTeContainersContainerIndex(topologyId, containerIndex, body, callback)</td>
    <td style="padding:15px">Update te-container</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-containers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyV2TopologyIdTeContainersContainerIndex(topologyId, containerIndex, callback)</td>
    <td style="padding:15px">delete thete-container</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-containers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTopologyV2TopologyIdTeContainersBulk(topologyId, body, callback)</td>
    <td style="padding:15px">Create a list of te-containers</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-containers/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTopologyV2TopologyIdTeContainersBulk(topologyId, body, callback)</td>
    <td style="padding:15px">Update a list of te-containers</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-containers/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTopologyV2TopologyIdTeContainersBulk(topologyId, body, callback)</td>
    <td style="padding:15px">Update a list of te-containers</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-containers/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyV2TopologyIdTeContainersBulk(topologyId, callback)</td>
    <td style="padding:15px">Delete a list of te-containers</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/te-containers/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdRoutes(topologyId, callback)</td>
    <td style="padding:15px">Provide a summary of discovered BGP routes</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdRoutesNodeIndex(topologyId, nodeIndex, callback)</td>
    <td style="padding:15px">List the IP routes on a given node</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/routes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTopologyV2TopologyIdPathComputation(topologyId, body, callback)</td>
    <td style="padding:15px">Compute a set of path</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/pathComputation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTopologyV2RpcOptimize(body, callback)</td>
    <td style="padding:15px">Trigger global or targeted optimization</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/rpc/optimize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2RpcOptimize(callback)</td>
    <td style="padding:15px">Get global optimization status</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/rpc/optimize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTopologyV2RpcSimulation(body, callback)</td>
    <td style="padding:15px">Failure Simulation optimization RPC</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/rpc/simulation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2RpcSimulation(callback)</td>
    <td style="padding:15px">Get Simulation optimization RPC list</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/rpc/simulation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2RpcSimulationUuid(uuid, callback)</td>
    <td style="padding:15px">Get a specific simulation</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/rpc/simulation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2RpcSimulationUuidReportName(uuid, reportName, callback)</td>
    <td style="padding:15px">Get a specific report of one simulation</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/rpc/simulation/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTopologyV2RpcDiverseTreeDesign(body, callback)</td>
    <td style="padding:15px">P2MP Diverse Tree Design optimization RPC</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/rpc/diverseTreeDesign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2RpcDiverseTreeDesign(callback)</td>
    <td style="padding:15px">Get Simulation optimization RPC list</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/rpc/diverseTreeDesign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2RpcDiverseTreeDesignUuid(uuid, callback)</td>
    <td style="padding:15px">Get a Specific Simulation optimization RPC Result</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/rpc/diverseTreeDesign/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedulerV2Tasklist(callback)</td>
    <td style="padding:15px">Gets all Tasks</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/v2/tasklist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedulerV2Tasks(callback)</td>
    <td style="padding:15px">Get tasks based on filter tasktype</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/v2/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSchedulerV2Addtask(body, callback)</td>
    <td style="padding:15px">Create a task</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/v2/addtask?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSchedulerV2Updatetask(body, callback)</td>
    <td style="padding:15px">Update a task</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/v2/updatetask?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSchedulerV2Removeaddtask(body, callback)</td>
    <td style="padding:15px">Remove and add a new Task</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/v2/removeaddtask?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSchedulerV2Deletetask(body, callback)</td>
    <td style="padding:15px">Delete tasks</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/v2/deletetask?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedulerV2TaskshistoryTaskId(taskId, callback)</td>
    <td style="padding:15px">Gets task execution status history in HTML table format for a given taskId</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/v2/taskshistory/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedulerV2TaskstatusTaskId(taskId, callback)</td>
    <td style="padding:15px">Gets task execution status history for a given taskId</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/v2/taskstatus/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConfig(body, callback)</td>
    <td style="padding:15px">Updates all component configurations</td>
    <td style="padding:15px">{base_path}/{version}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfig(body, callback)</td>
    <td style="padding:15px">Patch some component configurations</td>
    <td style="padding:15px">{base_path}/{version}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfig(callback)</td>
    <td style="padding:15px">DELETE configuration</td>
    <td style="padding:15px">{base_path}/{version}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportControllersV2(callback)</td>
    <td style="padding:15px">Get Transport Controller List</td>
    <td style="padding:15px">{base_path}/{version}/transportControllers/v2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransportControllersV2(body, callback)</td>
    <td style="padding:15px">Create Transport Controller Configuration</td>
    <td style="padding:15px">{base_path}/{version}/transportControllers/v2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportControllersV2TransportControllerIndex(transportControllerIndex, callback)</td>
    <td style="padding:15px">Get Transport Controller Configuration</td>
    <td style="padding:15px">{base_path}/{version}/transportControllers/v2/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTransportControllersV2TransportControllerIndex(transportControllerIndex, body, callback)</td>
    <td style="padding:15px">Update Transport Controller Configuration</td>
    <td style="padding:15px">{base_path}/{version}/transportControllers/v2/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTransportControllersV2TransportControllerIndex(transportControllerIndex, callback)</td>
    <td style="padding:15px">Delete Transport Controller Configuration</td>
    <td style="padding:15px">{base_path}/{version}/transportControllers/v2/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportControllerGroupsV2(callback)</td>
    <td style="padding:15px">Get Transport Controller Device Groups</td>
    <td style="padding:15px">{base_path}/{version}/transportControllerGroups/v2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransportControllerGroupsV2(body, callback)</td>
    <td style="padding:15px">Create A Transport Controller Device Group</td>
    <td style="padding:15px">{base_path}/{version}/transportControllerGroups/v2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTransportControllerGroupsV2(callback)</td>
    <td style="padding:15px">Delete a Transport Controller Device Group</td>
    <td style="padding:15px">{base_path}/{version}/transportControllerGroups/v2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportControllerGroupsV2TransportControllerGroupName(transportControllerGroupName, callback)</td>
    <td style="padding:15px">Get Transport Controller Device Groups</td>
    <td style="padding:15px">{base_path}/{version}/transportControllerGroups/v2/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTransportControllerGroupsV2TransportControllerGroupName(transportControllerGroupName, body, callback)</td>
    <td style="padding:15px">Update Devices in the Group</td>
    <td style="padding:15px">{base_path}/{version}/transportControllerGroups/v2/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTransportControllerGroupsV2TransportControllerGroupName(transportControllerGroupName, body, callback)</td>
    <td style="padding:15px">Create New Devices in a Group</td>
    <td style="padding:15px">{base_path}/{version}/transportControllerGroups/v2/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTransportControllerGroupsV2TransportControllerGroupName(transportControllerGroupName, callback)</td>
    <td style="padding:15px">Delete Devices in the Group</td>
    <td style="padding:15px">{base_path}/{version}/transportControllerGroups/v2/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetconfV2Profiles(callback)</td>
    <td style="padding:15px">Get all Profiles</td>
    <td style="padding:15px">{base_path}/{version}/netconf/v2/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetconfV2Profiles(body, callback)</td>
    <td style="padding:15px">Create New Profiles</td>
    <td style="padding:15px">{base_path}/{version}/netconf/v2/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNetconfV2Profiles(body, callback)</td>
    <td style="padding:15px">Update Profiles</td>
    <td style="padding:15px">{base_path}/{version}/netconf/v2/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetconfV2Profiles(callback)</td>
    <td style="padding:15px">Delete Profiles</td>
    <td style="padding:15px">{base_path}/{version}/netconf/v2/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetconfV2CollectionLiveNetwork(body, callback)</td>
    <td style="padding:15px">Create a New Collect</td>
    <td style="padding:15px">{base_path}/{version}/netconf/v2/collection/liveNetwork?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetconfV2CollectionCollectionJobId(collectionJobId, callback)</td>
    <td style="padding:15px">Get the Status of a Collection Job</td>
    <td style="padding:15px">{base_path}/{version}/netconf/v2/collection/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComponentConfig(callback)</td>
    <td style="padding:15px">Get Paragon PathFinder component configurations</td>
    <td style="padding:15px">{base_path}/{version}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdIpePolicy(topologyId, callback)</td>
    <td style="padding:15px">Get all IPE Policie</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/ipe/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTopologyV2TopologyIdIpePolicy(topologyId, body, callback)</td>
    <td style="padding:15px">Create IPE Policies</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/ipe/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTopologyV2TopologyIdIpePolicy(topologyId, body, callback)</td>
    <td style="padding:15px">Update IPE Policies</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/ipe/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyV2TopologyIdIpePolicy(topologyId, callback)</td>
    <td style="padding:15px">Delete IPE Policies</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/ipe/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyV2TopologyIdIpePolicyIpePolicyId(topologyId, ipePolicyId, callback)</td>
    <td style="padding:15px">Get single IPE policy</td>
    <td style="padding:15px">{base_path}/{version}/topology/v2/{pathv1}/ipe/policy/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
