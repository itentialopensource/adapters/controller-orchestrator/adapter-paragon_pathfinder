## Authenticating Paragon Pathfinder Adapter 

This document will go through the steps for authenticating the Paragon Pathfinder adapter with Dynamic Token Authentication and Multi-Step Authentication. Properly configuring the properties for an adapter in IAP is critical for getting the adapter online. You can read more about adapter authentication <a href="https://docs.itential.com/opensource/docs/authentication" target="_blank">HERE</a>. 

Companies periodically change authentication methods to provide better security. As this happens this section should be updated and contributed/merge back into the adapter repository.

### Dynamic Token Authentication
The Paragon Pathfinder adapter requires Dynamic Token Authentication. If you change authentication methods, you should change this section accordingly and merge it back into the adapter repository.

STEPS  
1. Ensure you have access to a Paragon Pathfinder server and that it is running
2. Follow the steps in the README.md to import the adapter into IAP if you have not already done so
3. Use the properties below for the ```properties.authentication``` field
```json
"authentication": {
  "auth_method": "request_token",
  "username": "<username>",
  "password": "<password>",
  "token_timeout": 600000,
  "token_cache": "local",
  "invalid_token_error": 401,
  "auth_field": "header.headers.x-iam-token",
  "auth_field_format": "{token}",
  "auth_logging": false
}
```
4. Use the properties below for the ```request.global_request.authData``` field
```json
"request": {
  "global_request": {
    "authData": {
      "user": {
        "name": "admin",
        "domain": "spadmin"
      },
      "methods": [
        "PASSWORD"
      ]
    }
  }
}
```
5. Restart the adapter. If your properties were set correctly, the adapter should go online. 

### Multi-Step Authentication
The Paragon Pathfinder adapter also supports Multi-Step Authentication using The Paragon IAM Authentication API. If you change authentication methods, you should change this section accordingly and merge it back into the adapter repository.

STEPS  
1. Ensure you have access to a Paragon Pathfinder server and that it is running
2. Follow the steps in the README.md to import the adapter into IAP if you have not already done so
3. Use the properties below for the ```properties.authentication``` field
```json
"authentication": {
  "auth_method": "multi_step_authentication",
  "multiStepAuthCalls": [
    {
      "name": "getFirstToken",
      "requestFields": {
        "user": {
          "domain": "",
          "name": "admin"
        },
        "password": "<password>",
        "methods": [
          "PASSWORD"
        ]
      },
      "responseFields": {
        "firstIdToken": "id_token",
        "scopeId": "scopes.0.id"
      },
      "successfullResponseCode": 200
    },
    {
      "name": "getSecondToken",
      "requestFields": {
        "token": "{getFirstToken.responseFields.firstIdToken}",
        "scopeId": "{getFirstToken.responseFields.scopeId}",
        "methods": [
          "TOKEN"
        ]
      },
      "responseFields": {
        "tokenp2": "id_token"
      },
      "successfullResponseCode": 200
    }
  ],
  "token_timeout": 600000,
  "token_cache": "local",
  "invalid_token_error": 401,
  "auth_field": "header.headers.x-iam-token",
  "auth_field_format": "{tokenp2}",
  "auth_logging": true
}
```
4. Restart the adapter. If your properties were set correctly, the adapter should go online. 

### Troubleshooting
- Make sure you copied over the correct username and password.
- Turn on debug level logs for the adapter in IAP Admin Essentials.
- Turn on auth_logging for the adapter in IAP Admin Essentials (adapter properties).
- Investigate the logs - in particular:
  - The FULL REQUEST log to make sure the proper headers are being sent with the request.
  - The FULL BODY log to make sure the payload is accurate.
  - The CALL RETURN log to see what the other system is telling us.
- Credentials should be ** masked ** by the adapter so make sure you verify the username and password - including that there are erroneous spaces at the front or end.
- Remember when you are done to turn auth_logging off as you do not want to log credentials.
