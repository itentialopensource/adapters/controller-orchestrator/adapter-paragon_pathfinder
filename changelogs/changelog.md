
## 1.2.8 [08-11-2023]

* Add calls and default value to schema files

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!12

---

## 1.2.7 [07-05-2023]

* Fix body objects for all calls

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!11

---

## 1.2.6 [05-31-2023]

* Patch/add query params

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!9

---

## 1.2.5 [05-25-2023]

* Patch/add query params

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!9

---

## 1.2.4 [04-27-2023]

* Patch/add msa

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!7

---

## 1.2.3 [04-27-2023]

* Patch/add msa

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!7

---

## 1.2.2 [04-25-2023]

* Fix body and token schema

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!6

---

## 1.2.1 [04-24-2023]

* Update system entity and sample properties

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!5

---

## 1.2.0 [04-22-2023]

* Remove ems endpoints

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!4

---

## 1.1.0 [04-21-2023]

* Remove iam endpoints

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!3

---

## 1.0.0 [04-21-2023]

* Add endpoints

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!2

---

## 0.1.1 [03-13-2023]

* Bug fixes and performance improvements

See commit e17c59a

---
