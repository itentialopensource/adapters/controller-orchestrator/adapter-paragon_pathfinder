
## 1.4.4 [10-14-2024]

* Changes made at 2024.10.14_19:38PM

See merge request itentialopensource/adapters/adapter-paragon_pathfinder!26

---

## 1.4.3 [09-14-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-paragon_pathfinder!24

---

## 1.4.2 [08-14-2024]

* Changes made at 2024.08.14_17:41PM

See merge request itentialopensource/adapters/adapter-paragon_pathfinder!23

---

## 1.4.1 [08-07-2024]

* Changes made at 2024.08.06_18:38PM

See merge request itentialopensource/adapters/adapter-paragon_pathfinder!22

---

## 1.4.0 [07-23-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!21

---

## 1.3.5 [03-28-2024]

* Changes made at 2024.03.28_13:32PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!20

---

## 1.3.4 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!19

---

## 1.3.3 [03-13-2024]

* Changes made at 2024.03.13_12:47PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!18

---

## 1.3.2 [03-11-2024]

* Changes made at 2024.03.11_15:53PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!17

---

## 1.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:19AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!16

---

## 1.3.0 [12-29-2023]

* Adapter Migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!14

---

## 1.2.8 [08-11-2023]

* Add calls and default value to schema files

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!12

---

## 1.2.7 [07-05-2023]

* Fix body objects for all calls

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!11

---

## 1.2.6 [05-31-2023]

* Patch/add query params

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!9

---

## 1.2.5 [05-25-2023]

* Patch/add query params

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!9

---

## 1.2.4 [04-27-2023]

* Patch/add msa

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!7

---

## 1.2.3 [04-27-2023]

* Patch/add msa

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!7

---

## 1.2.2 [04-25-2023]

* Fix body and token schema

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!6

---

## 1.2.1 [04-24-2023]

* Update system entity and sample properties

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!5

---

## 1.2.0 [04-22-2023]

* Remove ems endpoints

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!4

---

## 1.1.0 [04-21-2023]

* Remove iam endpoints

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!3

---

## 1.0.0 [04-21-2023]

* Add endpoints

See merge request itentialopensource/adapters/controller-orchestrator/adapter-paragon_pathfinder!2

---

## 0.1.1 [03-13-2023]

* Bug fixes and performance improvements

See commit e17c59a

---
